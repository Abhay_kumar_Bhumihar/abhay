import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:html/parser.dart';

import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/BoxdecorationCommon.dart';
import 'package:flutter_app/ChapterListFile/GetChapterOfBookPojo.dart';
import 'package:flutter_app/ChapterListFile/GetSubChapterPojo.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:flutter_html/html_parser.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_html/flutter_html.dart';
import 'dart:io' as io;
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import '../NewAdudioPractice.dart';

class SubChapterWithMeaniing extends StatefulWidget {
  SubChapterList chapterofbook;

  SubChapterWithMeaniing(SubChapterList chapterOfBook) {
    this.chapterofbook = chapterOfBook;
  }

  @override
  _SubChapterWithMeaningState createState() =>
      _SubChapterWithMeaningState(chapterofbook);
}

enum PlayerState { stopped, playing, paused }

class _SubChapterWithMeaningState extends State<SubChapterWithMeaniing> {
  SubChapterList chapterofbook;
  var TAG = "_HomePageState ";
  var colorCode = "0";
  var hindiCode = "0";
  Color color = Colors.grey;
  Color color1 = Colors.grey;
  Color textcolor1, textcolor2;
  Duration duration;
  Duration position;

  String directory;
  List file = new List();

  // var kUrl =
  //     "https://www.mediacollege.com/downloads/sound-effects/nature/forest/rainforest-ambient.mp3";
  AudioPlayer audioPlayer;
  String _filePath = "";

//"storage/emulated/0/Android/data/abhayfilepdf.mp3"
  String localFilePath;
  PlayerState playerState = PlayerState.stopped;

  get isPlaying => playerState == PlayerState.playing;

  get isPaused => playerState == PlayerState.paused;

  get durationText =>
      duration != null ? duration.toString().split('.').first : '';

  get positionText =>
      position != null ? position.toString().split('.').first : '';
  bool isMuted = false;

  var isdownloadfile = "";
  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;
  var isonline = true;
  var pathh = "";

  String stripExtension(final String s) {
    return s != null && s.lastIndexOf(".") > 0
        ? s.substring(0, s.lastIndexOf("."))
        : s;
  }

  String _parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString = parse(document.body.text).documentElement.text;
    return parsedString;
  }

  /*I get duration of sound file

second Word count in sentence

Divide sound duration by word count

and highlight each world in divided sec amount*/


  _SubChapterWithMeaningState(SubChapterList chapterId) {
    this.chapterofbook = chapterId;
  }

/*https://stackoverflow.com/questions/11667088/how-to-highlight-text-while-mediaplayer-is-playing-a-sound-in-android*/
  void tt() {
    Timer.periodic(Duration(microseconds: 1000), (_) {
      var aa = Bidi.stripHtmlIfNeeded(chapterofbook.chapterMeaningTextHindi);
      var percentage =
          int.parse(position.toString()) ~/ int.parse(duration.toString()) * 100;
      int count = aa.length;
      double aaa = int.parse(duration.toString()) / count;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    PreferenceUtils.init();
    setState(() {
      hindiCode = PreferenceUtils.getString("hindicode").toString();
      colorCode = PreferenceUtils.getString("themeColor").toString();
    });

    color = Color(hexStringToHexInt("#2A406C"));
    textcolor1 = Colors.white;
    textcolor2 = Colors.black;
    _listofFiles();

    debugPrint("DDD ${chapterofbook.chapterAudioPathHindi}");
    var a = "${stripExtension(chapterofbook.chapterAudioPathHindi)}";
    var b = a.replaceAll(" ", "");
    pathh = b.replaceAll("/", "");
    isdownloadfile =
        "/storage/emulated/0/Android/data/JainVaniAudio/" + pathh + ".mp3";
    isonline = true;
    _playLocal();
    initAudioPlayer();
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarColor: Colors.white,
    // ));
  }

  @override
  void dispose() {
    _positionSubscription.cancel();
    _audioPlayerStateSubscription.cancel();
    audioPlayer.stop();
    super.dispose();
  }

  void initAudioPlayer() {
    audioPlayer = AudioPlayer();
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => position = p));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        setState(() => duration = audioPlayer.duration);
      } else if (s == AudioPlayerState.STOPPED) {
        onComplete();
        setState(() {
          position = duration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = PlayerState.stopped;
        duration = Duration(seconds: 0);
        position = Duration(seconds: 0);
      });
    });
  }

  Future play() async {
    await audioPlayer.play(
        "http://ssjitservices.info/" + chapterofbook.chapterAudioPathHindi);
    setState(() {
      playerState = PlayerState.playing;
    });
  }

  Future _playLocal() async {
    //debugPrint("SDDSSD");
    for (int i = 0; i < file.length; i++) {
      var a = file[i].toString();
      var b = a.replaceAll("'", "");
      var c = b.replaceAll(" ", "");
      var d = c.substring(5);
      // debugPrint(c.substring(5));
      if (isdownloadfile.toString() == d) {
        //debugPrint("SDF SDF SDF DSF SDF ");
        localFilePath = d;
        isonline = false;
      } else {
        // debugPrint("ELSE CONDITION");
      }
    }
  }

  Future _playoflineOnlie() async {
    if (isonline) {
      debugPrint("ONLINE PLAYER");
      await audioPlayer.play(
          "http://ssjitservices.info/" + chapterofbook.chapterAudioPathHindi);
      setState(() {
        playerState = PlayerState.playing;
      });
    } else {
      debugPrint("OFFLINE PLAYER");
      await audioPlayer.play(localFilePath, isLocal: true);
      setState(() => playerState = PlayerState.playing);
    }
  }

  Future pause() async {
    await audioPlayer.pause();

    setState(() => playerState = PlayerState.paused);
  }

  Future stop() async {
    await audioPlayer.stop();
    setState(() {
      playerState = PlayerState.stopped;
      position = Duration();
    });
  }

  Future mute(bool muted) async {
    await audioPlayer.mute(muted);
    setState(() {
      isMuted = muted;
    });
  }

  void onComplete() {
    setState(() => playerState = PlayerState.stopped);
  }

  Future<Uint8List> _loadFileBytes(String url, {OnError onError}) async {
    Uint8List bytes;
    try {
      bytes = await readBytes(Uri.parse(url));
    } on ClientException {
      rethrow;
    }
    return bytes;
  }

  void _listofFiles() async {
    directory = (await getApplicationDocumentsDirectory()).path;
    setState(() {
      file = io.Directory("/storage/emulated/0/Android/data/JainVaniAudio")
          .listSync();
    });
  }

  Future<String> get _localDevicePath async {
    final _devicePath = await getExternalStorageDirectory();
    return _devicePath.path;
  }

  Future<File> _localFile({String path, String type}) async {
    String _path = await _localDevicePath;
    var _newPath =
        await Directory("/storage/emulated/0/Android/data/$path").create();
    localFilePath = "${_newPath.path}/$pathh.$type";
    return File("${_newPath.path}/$pathh.$type");
  }

  Future _downloadSampleVideo() async {
    Logger().w("File write complete. File Path");
    final _response = await http.get(Uri.parse(
        "http://ssjitservices.info/" + chapterofbook.chapterAudioPathHindi));
    if (_response.statusCode == 200) {
      final _file = await _localFile(type: "mp3", path: "JainVaniAudio");
      final _saveFile = await _file.writeAsBytes(_response.bodyBytes);
      Logger().w("File write complete. File Path ${_saveFile.path}");
      setState(() {
        _filePath = _saveFile.path;
        Navigator.of(context).pop();
        _listofFiles();
        _playLocal();
      });
    } else {
      Logger().e(_response.statusCode);
    }
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Downloding...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _playLocal();
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: Color(hexStringToHexInt("#6C6C88")),
        centerTitle: true,
        title: Text(
          "Jinvani",
          style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.12,
              color: Colors.white,
              fontFamily: "Poorich"),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Commonmethod.backgroundImage(colorCode)),
              fit: BoxFit.fill),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              //  Commonmethod.setData();
                              var shareval =
                                  PreferenceUtils.getString("themeColor")
                                      .toString();
                              print("$TAG  themeColor $shareval");

                              if (shareval == "0") {
                                PreferenceUtils.setString("themeColor", "1");
                                colorCode = "1";
                                debugPrint("111111111111");
                              } else
                              /* if (shareval == "1")*/ {
                                PreferenceUtils.setString("themeColor", "0");
                                debugPrint("OOOOOOOOOO");
                                colorCode = "0";
                              }
                              print("sfsdfsdf  " +
                                  PreferenceUtils.getString("themeColor")
                                      .toString());
                            });
                          },
                          child: Container(
                              height: 44,
                              width: 44,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/contrast.png'),
                                    fit: BoxFit.cover),
                              )),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Material(
                          color: Colors.grey,
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Row(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          PreferenceUtils.setString(
                                              "hindicode", "0");
                                          hindiCode = "0";
                                          color1 = Colors.grey;
                                          color = Color(
                                              hexStringToHexInt("#2A406C"));
                                          textcolor1 = Colors.white;
                                          textcolor2 = Colors.black;
                                        });
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: color,
                                        ),
                                        child: Center(
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              "English",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          PreferenceUtils.setString(
                                              "hindicode", "1");
                                          hindiCode = "1";
                                          color1 = Color(
                                              hexStringToHexInt("#2A406C"));
                                          color = Colors.grey;
                                          textcolor1 = Colors.black;
                                          textcolor2 = Colors.white;
                                        });
                                      },
                                      child: Container(
                                          decoration: BoxDecoration(
                                            color: color1,
                                          ),
                                          child: Center(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                "Hindi",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          )),
                                    )
                                  ],
                                )),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Html(
                  data: hindiCode == "0"
                      ? chapterofbook.chapterTextEnglish + ""
                      : chapterofbook.chapterTextHindi != null
                          ? chapterofbook.chapterTextHindi
                          : "N/A",
                  defaultTextStyle: TextStyle(
                      fontFamily: "Roboto Regular",
                      fontSize: MediaQuery.of(context).size.width * 0.05,
                      color: colorCode == "0" ? Colors.white : Colors.black),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * 0.1,
                      left: MediaQuery.of(context).size.width * 0.09,
                      right: MediaQuery.of(context).size.width * 0.09,
                      bottom: MediaQuery.of(context).size.width * 0.02),
                  child: _buildPlayer()

                  // GestureDetector(
                  //   onTap: () {
                  //     Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //           builder: (context) => AudioApp(
                  //               chapterofbook.chapterAudioPathHindi.toString())),
                  //     );
                  //   },
                  //   child: Container(
                  //     alignment: Alignment.center,
                  //     width: MediaQuery.of(context).size.width,
                  //     height: MediaQuery.of(context).size.height * 0.1 - 22,
                  //     child: Text(
                  //       "Play",
                  //       style: TextStyle(
                  //           fontFamily: "Roboto Regular",
                  //           fontSize: MediaQuery.of(context).size.width * 0.05,
                  //           color: Colors.white),
                  //     ),
                  //     decoration: Commonmethod.decoration(),
                  //   ),
                  // ),
                  ),
            ],
          ),
        ),
      ),
    ));
  }

  getTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String stringValue = prefs.getString('theme');
    return stringValue;
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }

  String getBackgroundAssetName() {
    if (getTheme() != null) {
      if (getTheme() == "0") return "assets/blue_background.png";
      if (getTheme() == "1") return "assets/white_background.png";
    }
    return 'assets/blue_background.png';
  }

  Widget _buildPlayer() => Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(mainAxisSize: MainAxisSize.min, children: [
              IconButton(
                onPressed: isPlaying ? null : () => _playoflineOnlie(),
                iconSize: 34.0,
                icon: Icon(Icons.play_arrow),
                color: Colors.cyan,
              ),
              IconButton(
                onPressed: isPlaying ? () => pause() : null,
                iconSize: 34.0,
                icon: Icon(Icons.pause),
                color: Colors.cyan,
              ),
              IconButton(
                onPressed: isPlaying || isPaused ? () => stop() : null,
                iconSize: 34.0,
                icon: Icon(Icons.stop),
                color: Colors.cyan,
              ),
              IconButton(
                onPressed: () {
                  if (isonline) {
                    _downloadSampleVideo();
                  }
                },
                iconSize: 34.0,
                icon: isonline == true
                    ? Icon(Icons.download_outlined)
                    : Icon(Icons.download_done_rounded),
                color: Colors.cyan,
              ),
            ]),
            if (duration != null)
              Slider(
                  value: position?.inMilliseconds?.toDouble() ?? 0.0,
                  onChanged: (double value) {
                    return audioPlayer.seek((value / 1000).roundToDouble());
                  },
                  min: 0.0,
                  max: duration.inMilliseconds.toDouble()),
            // if (position != null) _buildMuteButtons(),
            if (position != null) _buildProgressView()
          ],
        ),
      );

  Row _buildProgressView() => Row(mainAxisSize: MainAxisSize.min, children: [
        //TODO--- this is for circular progress bar Padding(
        //   padding: EdgeInsets.all(12.0),
        //   child: CircularProgressIndicator(
        //     value: position != null && position.inMilliseconds > 0
        //         ? (position?.inMilliseconds?.toDouble() ?? 0.0) /
        //             (duration?.inMilliseconds?.toDouble() ?? 0.0)
        //         : 0.0,
        //     valueColor: AlwaysStoppedAnimation(Colors.cyan),
        //     backgroundColor: Colors.grey.shade400,
        //   ),
        // ),
       /* Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            // Text(
            //   position != null
            //       ? "${positionText ?? ''} / ${durationText ?? ''}"
            //       : duration != null
            //       ? durationText
            //       : '',
            //   style: TextStyle(fontSize: 24.0),
            // ),
            Text(
              positionText,
              style: TextStyle(fontSize: 24.0),
            ),
            Text(
              durationText,
              style: TextStyle(fontSize: 24.0),
            )
          ],
        ),*/
      ]);
}
