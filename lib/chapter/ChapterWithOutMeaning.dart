import 'package:flutter/material.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../BoxdecorationCommon.dart';

class ChapterWithOutMeaning extends StatefulWidget {
  @override
  _ChapterWithOutMeaningState createState() => _ChapterWithOutMeaningState();
}

class _ChapterWithOutMeaningState extends State<ChapterWithOutMeaning> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    PreferenceUtils.init();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: Color(hexStringToHexInt("#6C6C88")),
        centerTitle: true,
        title: Text(
          "Jinvani",
          style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.12,
              color: Colors.white,
              fontFamily: "Poorich"),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Commonmethod.backgroundImage("0")),
              fit: BoxFit.fill),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          child: Material(
                            elevation: 10,
                            color: Color(hexStringToHexInt("#9F9CB7")),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.menu,
                                    color: Colors.black,
                                  ),
                                  Text(
                                    "Menu",
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  Commonmethod.chaptertext,
                  style: TextStyle(
                      color: PreferenceUtils.getString("themeColor") == "0"
                          ? Colors.white
                          : Colors.black,
                      fontFamily: "Roboto Light",
                      fontSize: 16.0),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.1,
                    left: MediaQuery.of(context).size.width * 0.09,
                    right: MediaQuery.of(context).size.width * 0.09,
                    bottom: MediaQuery.of(context).size.width * 0.02),
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.1 - 22,
                  child: Text(
                    "Play",
                    style: TextStyle(
                        fontFamily: "Roboto Regular",
                        fontSize: MediaQuery.of(context).size.width * 0.05,
                        color: Colors.white),
                  ),
                  decoration: Commonmethod.decoration(),
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  getTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String stringValue = prefs.getString('theme');
    return stringValue;
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }

  String getBackgroundAssetName() {
    if (getTheme() != null) {
      if (getTheme() == "0") return "assets/blue_background.png";
      if (getTheme() == "1") return "assets/white_background.png";
    }
    return 'assets/blue_background.png';
  }
}
