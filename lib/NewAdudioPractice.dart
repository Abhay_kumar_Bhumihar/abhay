import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'BoxdecorationCommon.dart';
import 'dart:io' as io;

typedef void OnError(Exception exception);

// void main() {
//   runApp(MaterialApp(home: Scaffold(body: AudioApp())));
// }

enum PlayerState { stopped, playing, paused }

class AudioApp extends StatefulWidget {
  String data;

  AudioApp(String data) {
    this.data = data;
  }

  @override
  _AudioAppState createState() => _AudioAppState(data);
}

class _AudioAppState extends State<AudioApp> {
  String data;

  _AudioAppState(String data) {
    this.data = data;
  }

  Duration duration;
  Duration position;

  String directory;
  List file = new List();

  // var kUrl =
  //     "https://www.mediacollege.com/downloads/sound-effects/nature/forest/rainforest-ambient.mp3";
  AudioPlayer audioPlayer;
  String _filePath = "";

//"storage/emulated/0/Android/data/abhayfilepdf.mp3"
  String localFilePath;

  PlayerState playerState = PlayerState.stopped;

  get isPlaying => playerState == PlayerState.playing;

  get isPaused => playerState == PlayerState.paused;

  get durationText =>
      duration != null ? duration.toString().split('.').first : '';

  get positionText =>
      position != null ? position.toString().split('.').length : '';

  bool isMuted = false;

  var isdownloadfile = "";
  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;
  var TAG = "_audioPlayerPage ";
  var colorCode = "0";
  var isonline = true;
  var pathh = "";

  String stripExtension(final String s) {
    return s != null && s.lastIndexOf(".") > 0
        ? s.substring(0, s.lastIndexOf("."))
        : s;
  }

  @override
  void initState() {
    super.initState();
    debugPrint("DATA______ ${stripExtension(data)}");
    PreferenceUtils.init();
    debugPrint("$TAG shared pref themeColor=" +
        PreferenceUtils.getString("themeColor").toString());
    setState(() {
      colorCode = PreferenceUtils.getString("themeColor").toString();
    });
    _listofFiles();

    var a = "${stripExtension(data)}";
    var b = a.replaceAll(" ", "");
    pathh = b.replaceAll("/", "");
    isdownloadfile =
        "/storage/emulated/0/Android/data/JainVaniAudio/" + pathh + ".mp3";
    isonline = true;
    _playLocal();
    initAudioPlayer();
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarColor: Colors.white,
    // ));
  }

  @override
  void dispose() {
    _positionSubscription.cancel();
    _audioPlayerStateSubscription.cancel();
    audioPlayer.stop();
    super.dispose();
  }

  void initAudioPlayer() {
    audioPlayer = AudioPlayer();
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => position = p));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        setState(() => duration = audioPlayer.duration);
      } else if (s == AudioPlayerState.STOPPED) {
        onComplete();
        setState(() {
          position = duration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = PlayerState.stopped;
        duration = Duration(seconds: 0);
        position = Duration(seconds: 0);
      });
    });
  }

  Future play() async {
    await audioPlayer.play("http://ssjitservices.info/" + data);
    setState(() {
      playerState = PlayerState.playing;
    });
  }

  Future _playLocal() async {
    debugPrint("SDDSSD");
    for (int i = 0; i < file.length; i++) {
      var a = file[i].toString();
      var b = a.replaceAll("'", "");
      var c = b.replaceAll(" ", "");
      var d = c.substring(5);
      debugPrint(c.substring(5));
      if (isdownloadfile.toString() == d) {
        debugPrint("SDF SDF SDF DSF SDF ");
        localFilePath = d;
        isonline = false;
      } else {
        debugPrint("ELSE CONDITION");
      }
    }
  }

  Future _playoflineOnlie() async {
    if (isonline) {
      debugPrint("ONLINE PLAYER");
      await audioPlayer.play("http://ssjitservices.info/" + data);
      setState(() {
        playerState = PlayerState.playing;
      });
    } else {
      debugPrint("OFFLINE PLAYER");
      await audioPlayer.play(localFilePath, isLocal: true);
      setState(() => playerState = PlayerState.playing);
    }
  }

  Future pause() async {
    await audioPlayer.pause();
    setState(() => playerState = PlayerState.paused);
  }

  Future stop() async {
    await audioPlayer.stop();
    setState(() {
      playerState = PlayerState.stopped;
      position = Duration();
    });
  }

  Future mute(bool muted) async {
    await audioPlayer.mute(muted);
    setState(() {
      isMuted = muted;
    });
  }

  void onComplete() {
    setState(() => playerState = PlayerState.stopped);
  }

  Future<Uint8List> _loadFileBytes(String url, {OnError onError}) async {
    Uint8List bytes;
    try {
      bytes = await readBytes(Uri.parse(url));
    } on ClientException {
      rethrow;
    }
    return bytes;
  }

  void _listofFiles() async {
   try{
     directory = (await getApplicationDocumentsDirectory()).path;
     setState(() {
       file = io.Directory("/storage/emulated/0/Android/data/JainVaniAudio")
           .listSync();
     });
   }catch(e){

   }
  }

  Future<String> get _localDevicePath async {
    final _devicePath = await getExternalStorageDirectory();
    return _devicePath.path;
  }

  Future<File> _localFile({String path, String type}) async {
    String _path = await _localDevicePath;

    var _newPath =
        await Directory("/storage/emulated/0/Android/data/$path").create();
    localFilePath = "${_newPath.path}/$pathh.$type";
    return File("${_newPath.path}/$pathh.$type");
  }

  Future _downloadSampleVideo(BuildContext context) async {
    showLoaderDialog(context);
    Logger().w("File write complete. File Path");
    final _response =
        await http.get(Uri.parse("http://ssjitservices.info/" + data));
    if (_response.statusCode == 200) {
      final _file = await _localFile(type: "mp3", path: "JainVaniAudio");
      final _saveFile = await _file.writeAsBytes(_response.bodyBytes);
      Logger().w("File write complete. File Path ${_saveFile.path}");
      setState(() {
        _filePath = _saveFile.path;
        Navigator.of(context).pop();
        _listofFiles();
        _playLocal();
      });
    } else {
      Logger().e(_response.statusCode);
    }
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Downloding...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _playLocal();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(hexStringToHexInt("#6C6C88")),
        centerTitle: true,
        title: Text(
          "Jinvani",
          style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.12,
              color: Colors.white,
              fontFamily: "Poorich"),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Commonmethod.backgroundImage(colorCode)),
              fit: BoxFit.fill),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Commonmethod.logowithTextformediaplayer(Colors.black, context),

            _buildPlayer(),
            // if (!kIsWeb)
            //   localFilePath != null ? Text(localFilePath) : Container(),
            // if (!kIsWeb)
            //   Padding(
            //     padding: const EdgeInsets.all(8.0),
            //     child: Row(
            //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //       children: [
            //         RaisedButton(
            //           onPressed: () => _downloadSampleVideo(),
            //           child: Text('Download'),
            //         ),
            //         //if (localFilePath != null)
            //         RaisedButton(
            //           onPressed: () => _playLocal(),
            //           child: Text('play local'),
            //         ),
            //       ],
            //     ),
            //   ),
          ],
        ),
      ),
    );
  }

  Widget _buildPlayer() => Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(mainAxisSize: MainAxisSize.min, children: [
              IconButton(
                onPressed: isPlaying ? null : () => _playoflineOnlie(),
                iconSize: 34.0,
                icon: Icon(Icons.play_arrow),
                color: Colors.cyan,
              ),
              IconButton(
                onPressed: isPlaying ? () => pause() : null,
                iconSize: 34.0,
                icon: Icon(Icons.pause),
                color: Colors.cyan,
              ),
              IconButton(
                onPressed: isPlaying || isPaused ? () => stop() : null,
                iconSize: 34.0,
                icon: Icon(Icons.stop),
                color: Colors.cyan,
              ),
              IconButton(
                onPressed: () {
                  if (isonline) {
                    _downloadSampleVideo(context);
                  }
                },
                iconSize: 34.0,
                icon: isonline == true
                    ? Icon(Icons.download_outlined)
                    : Icon(Icons.download_done_rounded),
                color: Colors.cyan,
              ),
            ]),
            if (duration != null)
              Slider(
                  value: position?.inMilliseconds?.toDouble() ?? 0.0,
                  onChanged: (double value) {
                    return audioPlayer.seek((value / 1000).roundToDouble());
                  },
                  min: 0.0,
                  max: duration.inMilliseconds.toDouble()),
            // if (position != null) _buildMuteButtons(),
            if (position != null) _buildProgressView()
          ],
        ),
      );

  Row _buildProgressView() => Row(mainAxisSize: MainAxisSize.min, children: [
        //TODO--- this is for circular progress bar Padding(
        //   padding: EdgeInsets.all(12.0),
        //   child: CircularProgressIndicator(
        //     value: position != null && position.inMilliseconds > 0
        //         ? (position?.inMilliseconds?.toDouble() ?? 0.0) /
        //             (duration?.inMilliseconds?.toDouble() ?? 0.0)
        //         : 0.0,
        //     valueColor: AlwaysStoppedAnimation(Colors.cyan),
        //     backgroundColor: Colors.grey.shade400,
        //   ),
        // ),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: <Widget>[
        //     // Text(
        //     //   position != null
        //     //       ? "${positionText ?? ''} / ${durationText ?? ''}"
        //     //       : duration != null
        //     //       ? durationText
        //     //       : '',
        //     //   style: TextStyle(fontSize: 24.0),
        //     // ),
        //     Text(
        //       positionText,
        //       style: TextStyle(fontSize: 24.0),
        //     ),
        //     Text(
        //       durationText,
        //       style: TextStyle(fontSize: 24.0),
        //     )
        //   ],
        // ),
      ]);

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }
}
