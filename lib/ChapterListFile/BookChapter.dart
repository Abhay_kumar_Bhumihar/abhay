import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/BoxdecorationCommon.dart';
import 'package:flutter_app/ChapterListFile/SubChapter.dart';
import 'package:flutter_app/audioplay/PlayAudioPage.dart';
import 'package:flutter_app/chapter/ChapterWithMeaning.dart';
import 'package:flutter_app/homepage/PracticePerfomPage.dart';
import 'package:flutter_app/homepage/SubAudioListPage.dart';
import 'package:flutter_app/networkRequest/ApiUrl.dart';
import 'package:flutter_app/networkRequest/Webapi.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:flutter_app/sharedfile/databasehelper.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'GetChapterOfBookPojo.dart';

class BookChapter extends StatefulWidget {
  var bookId = "";

  BookChapter(String bookId) {
    this.bookId = bookId;
  }

  @override
  _BookChapterState createState() => _BookChapterState(bookId);
}

class _BookChapterState extends State<BookChapter> {
  Color color = Colors.grey;
  Color color1 = Colors.grey;
  Color textcolor1, textcolor2;
  GetChapterOfBookPojo _chapterOfBookPojo;
  List<ChapterOfBook> _chapterofBookliist = [];
  final dbhelper = DataBaseHelper.instance;
  var bookId = "";
  var hindiCode = "0";
  var colorCode = "0";

  _BookChapterState(String bookId) {
    this.bookId = bookId;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    PreferenceUtils.init();
    setState(() {
      hindiCode = PreferenceUtils.getString("hindicode").toString();
      colorCode = PreferenceUtils.getString("themeColor").toString();
    });
    Webapi().check().then((intenet) {
      if (intenet != null && intenet) {
        _getChapterList(context);
      } else {
        debugPrint("No internet , fetch data from local");
        getBookChapterfromDb(bookId);
      }
    });

  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(hexStringToHexInt("#6C6C88")),
          centerTitle: true,
          title: Text(
            "Jinvani",
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.12,
                color: Colors.white,
                fontFamily: "Poorich"),
          ),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(Commonmethod.backgroundImage(colorCode)),
                fit: BoxFit.fill),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   children: <Widget>[
                //     Padding(
                //       padding: const EdgeInsets.all(8.0),
                //       child: Row(
                //         children: <Widget>[
                //           GestureDetector(
                //             onTap: () {
                //               setState(() {
                //                 Commonmethod.setData();
                //                 print(PreferenceUtils.getString("themeColor"));
                //               });
                //             },
                //             child: Container(
                //               child: Material(
                //                 elevation: 10,
                //                 color: Color(hexStringToHexInt("#9F9CB7")),
                //                 shape: RoundedRectangleBorder(
                //                     borderRadius: BorderRadius.circular(5.0)),
                //                 child: Padding(
                //                   padding: const EdgeInsets.all(8.0),
                //                   child: Row(
                //                     children: <Widget>[
                //                       Icon(
                //                         Icons.menu,
                //                         color: Colors.black,
                //                       ),
                //                       Text(
                //                         "Menu",
                //                       )
                //                     ],
                //                   ),
                //                 ),
                //               ),
                //             ),
                //           )
                //         ],
                //       ),
                //     ),
                //     Padding(
                //       padding: const EdgeInsets.all(8.0),
                //       child: Row(
                //         children: <Widget>[
                //           Material(
                //             color: Colors.grey,
                //             borderRadius:
                //                 BorderRadius.all(Radius.circular(5.0)),
                //             child: Padding(
                //               padding: const EdgeInsets.all(2.0),
                //               child: Container(
                //                   decoration: BoxDecoration(
                //                     color: Colors.transparent,
                //                   ),
                //                   child: Row(
                //                     children: <Widget>[
                //                       GestureDetector(
                //                         onTap: () {
                //                           setState(() {
                //                             color1 = Colors.grey;
                //                             color = Color(
                //                                 hexStringToHexInt("#2A406C"));
                //                             textcolor1 = Colors.white;
                //                             textcolor2 = Colors.black;
                //                           });
                //                         },
                //                         child: Container(
                //                           decoration: BoxDecoration(
                //                             color: color,
                //                           ),
                //                           child: Center(
                //                             child: Padding(
                //                               padding:
                //                                   const EdgeInsets.all(8.0),
                //                               child: Text(
                //                                 "English",
                //                                 style: TextStyle(
                //                                     color: textcolor2),
                //                               ),
                //                             ),
                //                           ),
                //                         ),
                //                       ),
                //                       GestureDetector(
                //                         onTap: () {
                //                           setState(() {
                //                             color1 = Color(
                //                                 hexStringToHexInt("#2A406C"));
                //                             color = Colors.grey;
                //                             textcolor1 = Colors.black;
                //                             textcolor2 = Colors.white;
                //                           });
                //                         },
                //                         child: Container(
                //                             decoration: BoxDecoration(
                //                               color: color1,
                //                             ),
                //                             child: Center(
                //                               child: Padding(
                //                                 padding:
                //                                     const EdgeInsets.all(8.0),
                //                                 child: Text(
                //                                   "Hindi",
                //                                   style: TextStyle(
                //                                       color: textcolor1),
                //                                 ),
                //                               ),
                //                             )),
                //                       )
                //                     ],
                //                   )),
                //             ),
                //           )
                //         ],
                //       ),
                //     )
                //   ],
                // ),
                SizedBox(
                  height: MediaQuery.of(context).size.width * 0.2,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.6,
                  child: _chpterListview(context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }

  void _getChapterList(BuildContext context) async {
    showLoaderDialog(context);
    Map map = {"BookID": bookId};

    String response =
        await Webapi().postWithbody(ApiUrl.Get_Chapter_ofBook, map);

    setState(() {
      Navigator.of(context).pop();
      var jsondata = json.decode(response);
      var message = jsondata['Message'];
      if (message.toString() == "Success") {
        PreferenceUtils.setUserPojo("_getChapterList", response);
        debugPrint("AP DATA $response");
        _chapterOfBookPojo = getChapterOfBookPojoFromJson(response);
        _chapterofBookliist = _chapterOfBookPojo.data;

        for (int i = 0; i < _chapterofBookliist.length; i++) {
          saveBookChapter(
              _chapterofBookliist[i].id,
              _chapterofBookliist[i].bookId,
              _chapterofBookliist[i].chapterNameEnglish == null
                  ? ""
                  : _chapterofBookliist[i].chapterNameEnglish,
              _chapterofBookliist[i].chapterNameHindi == null
                  ? ""
                  : _chapterofBookliist[i].chapterNameHindi,
              _chapterofBookliist[i].chapterTextEnglish == null
                  ? ""
                  : _chapterofBookliist[i].chapterTextEnglish,
              _chapterofBookliist[i].chapterTextHindi == null
                  ? ""
                  : _chapterofBookliist[i].chapterTextHindi,
              _chapterofBookliist[i].chapterMeaningTextEnglish == null
                  ? ""
                  : _chapterofBookliist[i].chapterMeaningTextEnglish,
              _chapterofBookliist[i].chapterMeaningTextHindi == null
                  ? ""
                  : _chapterofBookliist[i].chapterMeaningTextHindi,
              _chapterofBookliist[i].chapterAudioPathEnglish == null
                  ? ""
                  : _chapterofBookliist[i].chapterAudioPathEnglish,
              _chapterofBookliist[i].chapterAudioPathHindi == null
                  ? ""
                  : _chapterofBookliist[i].chapterAudioPathHindi,
              _chapterofBookliist[i].isSubChapter.toString() == null
                  ? ""
                  : _chapterofBookliist[i].isSubChapter.toString());
        }
      } else {
        debugPrint("ERROR ERROR $message");
      }
    });
  }

  Widget _chpterListview(context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          itemCount: _chapterofBookliist.length,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemBuilder: (context, position) {
            return Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.09,
                  right: MediaQuery.of(context).size.width * 0.09,
                  bottom: MediaQuery.of(context).size.width * 0.02),
              child: GestureDetector(
                onTap: () {
                  if (_chapterofBookliist[position].isSubChapter == true) {
                    debugPrint(
                        "GO FOR SUB CHAPTER ${_chapterofBookliist[position].isSubChapter}");
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SubChapterpage(
                              _chapterofBookliist[position].id.toString()),
                        ));
                  } else {
                    debugPrint("GO FOR  CHAPTER");
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              ChapterWithMeaning(_chapterofBookliist[position]),
                        ));
                    /*todo---yaha pe check krna hai ki user to practice se aaya hai ya to perform se
                    *  agr to practice se aaya hai to With meaning wale nhi to WIthout Meaning wale pe*/
                  }
                },
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.1 - 22,
                  child: Text(
                    hindiCode == "0"
                        ? _chapterofBookliist[position].chapterNameEnglish + ""
                        : _chapterofBookliist[position].chapterNameHindi != null
                            ? _chapterofBookliist[position].chapterNameHindi
                            : "N/A",
                    style: TextStyle(
                        fontFamily: "Roboto Regular",
                        fontSize: MediaQuery.of(context).size.width * 0.05,
                        color: Colors.white),
                  ),
                  decoration: Commonmethod.decoration(),
                ),
              ),
            );
          }),
    );
  }

  void saveBookChapter(
      int chapterid,
      int bookid,
      String chapterEnglishname,
      String chapternameHindi,
      String chaptetTextenglish,
      String chapterTextHindi,
      String chapterMeaningEnglish,
      String chapterMeaningHindi,
      String chapterAudiopathEng,
      String chapterAudioHindi,
      String isSubchapter) async {
    Map<String, dynamic> row = {
      DataBaseHelper.CHAPTER_LIST_ID: chapterid,
      DataBaseHelper.CHAPTER_BOOK_ID: bookid,
      DataBaseHelper.CHAPTER_LIST_ChapterNameEnglish: chapterEnglishname,
      DataBaseHelper.CHAPTER_LIST_ChapterNameHindi: chapternameHindi,
      DataBaseHelper.CHAPTER_LIST_ChapterTextEnglish: chaptetTextenglish,
      DataBaseHelper.CHAPTER_LIST_ChapterTextHindi: chapterTextHindi,
      DataBaseHelper.CHAPTER_LIST_ChapterMeaningTextEnglish:
          chapterMeaningEnglish,
      DataBaseHelper.CHAPTER_LIST_ChapterMeaningTextHindi: chapterMeaningHindi,
      DataBaseHelper.CHAPTER_LIST_ChapterAudioPathEnglish: chapterAudiopathEng,
      DataBaseHelper.CHAPTER_LIST_ChapterChapterAudioPathHindi:
          chapterAudioHindi,
      DataBaseHelper.CHAPTER_LIST_ChapterIsSubChapter: isSubchapter
    };
    var id = await dbhelper.insert(DataBaseHelper.table_CHAPTER_LIST, row);
    debugPrint("CHAPTER  SAVE $id");
  }

  void getBookChapterfromDb(bookid) async {
    var response = await dbhelper.getBookChapter(int.parse(bookId));
    setState(() {
      _chapterofBookliist = response;
    });
  }
}
