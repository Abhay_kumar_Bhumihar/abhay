// To parse this JSON data, do
//
//     final getChapterOfBookPojo = getChapterOfBookPojoFromJson(jsonString);

import 'dart:convert';

GetChapterOfBookPojo getChapterOfBookPojoFromJson(String str) =>
    GetChapterOfBookPojo.fromJson(json.decode(str));

String getChapterOfBookPojoToJson(GetChapterOfBookPojo data) =>
    json.encode(data.toJson());

class GetChapterOfBookPojo {
  GetChapterOfBookPojo({
    this.message,
    this.data,
    this.userId,
  });

  String message;
  List<ChapterOfBook> data;
  int userId;

  factory GetChapterOfBookPojo.fromJson(Map<String, dynamic> json) =>
      GetChapterOfBookPojo(
        message: json["Message"],
        data: List<ChapterOfBook>.from(
            json["Data"].map((x) => ChapterOfBook.fromJson(x))),
        userId: json["userId"],
      );

  Map<String, dynamic> toJson() => {
        "Message": message,
        "Data": List<dynamic>.from(data.map((x) => x.toJson())),
        "userId": userId,
      };
}

class ChapterOfBook {
  ChapterOfBook({
    this.id,
    this.bookId,
    this.chapterNameEnglish,
    this.chapterNameHindi,
    this.chapterTextEnglish,
    this.chapterTextHindi,
    this.chapterMeaningTextEnglish,
    this.chapterMeaningTextHindi,
    this.chapterAudioPathEnglish,
    this.chapterAudioPathHindi,
    this.isSubChapter,
  });

  int id;
  int bookId;
  String chapterNameEnglish;
  String chapterNameHindi;
  String chapterTextEnglish;
  String chapterTextHindi;
  dynamic chapterMeaningTextEnglish;
  String chapterMeaningTextHindi;
  String chapterAudioPathEnglish;
  String chapterAudioPathHindi;
  bool isSubChapter;

  factory ChapterOfBook.fromJson(Map<String, dynamic> json) => ChapterOfBook(
        id: json["ID"],
        bookId: json["BookID"],
        chapterNameEnglish: json["ChapterNameEnglish"],
        chapterNameHindi:
            json["ChapterNameHindi"] == null ? null : json["ChapterNameHindi"],
        chapterTextEnglish: json["ChapterTextEnglish"] == null
            ? null
            : json["ChapterTextEnglish"],
        chapterTextHindi:
            json["ChapterTextHindi"] == null ? null : json["ChapterTextHindi"],
        chapterMeaningTextEnglish: json["ChapterMeaningTextEnglish"],
        chapterMeaningTextHindi: json["ChapterMeaningTextHindi"] == null
            ? null
            : json["ChapterMeaningTextHindi"],
        chapterAudioPathEnglish: json["ChapterAudioPathEnglish"],
        chapterAudioPathHindi: json["ChapterAudioPathHindi"],
        isSubChapter:
            json["IsSubChapter"] == null ? null : json["IsSubChapter"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "BookID": bookId,
        "ChapterNameEnglish": chapterNameEnglish,
        "ChapterNameHindi": chapterNameHindi == null ? null : chapterNameHindi,
        "ChapterTextEnglish":
            chapterTextEnglish == null ? null : chapterTextEnglish,
        "ChapterTextHindi": chapterTextHindi == null ? null : chapterTextHindi,
        "ChapterMeaningTextEnglish": chapterMeaningTextEnglish,
        "ChapterMeaningTextHindi":
            chapterMeaningTextHindi == null ? null : chapterMeaningTextHindi,
        "ChapterAudioPathEnglish": chapterAudioPathEnglish,
        "ChapterAudioPathHindi": chapterAudioPathHindi,
        "IsSubChapter": isSubChapter == null ? null : isSubChapter,
      };
}
