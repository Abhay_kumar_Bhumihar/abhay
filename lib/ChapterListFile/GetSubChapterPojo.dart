// To parse this JSON data, do
//
//     final getSubChapterPojo = getSubChapterPojoFromJson(jsonString);

import 'dart:convert';

GetSubChapterPojo getSubChapterPojoFromJson(String str) =>
    GetSubChapterPojo.fromJson(json.decode(str));

String getSubChapterPojoToJson(GetSubChapterPojo data) =>
    json.encode(data.toJson());

class GetSubChapterPojo {
  GetSubChapterPojo({
    this.message,
    this.data,
    this.userId,
  });

  String message;
  List<SubChapterList> data;
  int userId;

  factory GetSubChapterPojo.fromJson(Map<String, dynamic> json) =>
      GetSubChapterPojo(
        message: json["Message"],
        data: List<SubChapterList>.from(
            json["Data"].map((x) => SubChapterList.fromJson(x))),
        userId: json["userId"],
      );

  Map<String, dynamic> toJson() => {
        "Message": message,
        "Data": List<dynamic>.from(data.map((x) => x.toJson())),
        "userId": userId,
      };
}

class SubChapterList {
  SubChapterList({
    this.id,
    this.chapterId,
    this.chapterNameEnglish,
    this.chapterNameHindi,
    this.chapterTextEnglish,
    this.chapterTextHindi,
    this.chapterMeaningTextEnglish,
    this.chapterMeaningTextHindi,
    this.chapterAudioPathEnglish,
    this.chapterAudioPathHindi,
  });

  int id;
  int chapterId;
  String chapterNameEnglish;
  String chapterNameHindi;
  String chapterTextEnglish;
  String chapterTextHindi;
  dynamic chapterMeaningTextEnglish;
  String chapterMeaningTextHindi;
  String chapterAudioPathEnglish;
  String chapterAudioPathHindi;

  factory SubChapterList.fromJson(Map<String, dynamic> json) => SubChapterList(
        id: json["ID"],
        chapterId: json["ChapterID"],
        chapterNameEnglish: json["ChapterNameEnglish"],
        chapterNameHindi: json["ChapterNameHindi"],
        chapterTextEnglish: json["ChapterTextEnglish"],
        chapterTextHindi: json["ChapterTextHindi"],
        chapterMeaningTextEnglish: json["ChapterMeaningTextEnglish"],
        chapterMeaningTextHindi: json["ChapterMeaningTextHindi"],
        chapterAudioPathEnglish: json["ChapterAudioPathEnglish"],
        chapterAudioPathHindi: json["ChapterAudioPathHindi"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "ChapterID": chapterId,
        "ChapterNameEnglish": chapterNameEnglish,
        "ChapterNameHindi": chapterNameHindi,
        "ChapterTextEnglish": chapterTextEnglish,
        "ChapterTextHindi": chapterTextHindi,
        "ChapterMeaningTextEnglish": chapterMeaningTextEnglish,
        "ChapterMeaningTextHindi": chapterMeaningTextHindi,
        "ChapterAudioPathEnglish": chapterAudioPathEnglish,
        "ChapterAudioPathHindi": chapterAudioPathHindi,
      };
}
