import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/BoxdecorationCommon.dart';
import 'package:flutter_app/audioplay/PlayAudioPage.dart';
import 'package:flutter_app/chapter/ChapterWithMeaning.dart';
import 'package:flutter_app/chapter/SubChapterWithMeaning.dart';
import 'package:flutter_app/homepage/PracticePerfomPage.dart';
import 'package:flutter_app/homepage/SubAudioListPage.dart';
import 'package:flutter_app/networkRequest/ApiUrl.dart';
import 'package:flutter_app/networkRequest/Webapi.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:flutter_app/sharedfile/databasehelper.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'GetChapterOfBookPojo.dart';
import 'GetSubChapterPojo.dart';

class SubChapterpage extends StatefulWidget {
  var chapterId = "";

  SubChapterpage(String chapterId) {
    this.chapterId = chapterId;
  }

  @override
  _SubChapterState createState() => _SubChapterState(chapterId);
}

class _SubChapterState extends State<SubChapterpage> {
  Color color = Colors.grey;
  Color color1 = Colors.grey;
  Color textcolor1, textcolor2;
  GetSubChapterPojo _subchapterOfBookPojo;
  List<SubChapterList> _subchapterofBookliist = [];
  final dbhelper = DataBaseHelper.instance;
  var hindiCode = "0";
  var chapterId = "";
  var colorCode = "0";

  _SubChapterState(String chapterId) {
    this.chapterId = chapterId;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    PreferenceUtils.init();
    setState(() {
      hindiCode = PreferenceUtils.getString("hindicode").toString();
      colorCode = PreferenceUtils.getString("themeColor").toString();
    });
    Webapi().check().then((intenet) {
      if (intenet != null && intenet) {
        _getSubChapterList(context);
      } else {
        getAllSubChapter(chapterId);
        debugPrint("No internet , fetch data from local");
      }
    });
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarColor: Colors.white,
    // ));
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(hexStringToHexInt("#6C6C88")),
          centerTitle: true,
          title: Text(
            "Jinvani",
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.12,
                color: Colors.white,
                fontFamily: "Poorich"),
          ),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(Commonmethod.backgroundImage(colorCode)),
                fit: BoxFit.fill),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   children: <Widget>[
                //     Padding(
                //       padding: const EdgeInsets.all(8.0),
                //       child: Row(
                //         children: <Widget>[
                //           GestureDetector(
                //             onTap: () {
                //               setState(() {
                //                 Commonmethod.setData();
                //                 print(PreferenceUtils.getString("themeColor"));
                //               });
                //             },
                //             child: Container(
                //               child: Material(
                //                 elevation: 10,
                //                 color: Color(hexStringToHexInt("#9F9CB7")),
                //                 shape: RoundedRectangleBorder(
                //                     borderRadius: BorderRadius.circular(5.0)),
                //                 child: Padding(
                //                   padding: const EdgeInsets.all(8.0),
                //                   child: Row(
                //                     children: <Widget>[
                //                       Icon(
                //                         Icons.menu,
                //                         color: Colors.black,
                //                       ),
                //                       Text(
                //                         "Menu",
                //                       )
                //                     ],
                //                   ),
                //                 ),
                //               ),
                //             ),
                //           )
                //         ],
                //       ),
                //     ),
                //     Padding(
                //       padding: const EdgeInsets.all(8.0),
                //       child: Row(
                //         children: <Widget>[
                //           Material(
                //             color: Colors.grey,
                //             borderRadius:
                //                 BorderRadius.all(Radius.circular(5.0)),
                //             child: Padding(
                //               padding: const EdgeInsets.all(2.0),
                //               child: Container(
                //                   decoration: BoxDecoration(
                //                     color: Colors.transparent,
                //                   ),
                //                   child: Row(
                //                     children: <Widget>[
                //                       GestureDetector(
                //                         onTap: () {
                //                           setState(() {
                //                             color1 = Colors.grey;
                //                             color = Color(
                //                                 hexStringToHexInt("#2A406C"));
                //                             textcolor1 = Colors.white;
                //                             textcolor2 = Colors.black;
                //                           });
                //                         },
                //                         child: Container(
                //                           decoration: BoxDecoration(
                //                             color: color,
                //                           ),
                //                           child: Center(
                //                             child: Padding(
                //                               padding:
                //                                   const EdgeInsets.all(8.0),
                //                               child: Text(
                //                                 "English",
                //                                 style: TextStyle(
                //                                     color: textcolor2),
                //                               ),
                //                             ),
                //                           ),
                //                         ),
                //                       ),
                //                       GestureDetector(
                //                         onTap: () {
                //                           setState(() {
                //                             color1 = Color(
                //                                 hexStringToHexInt("#2A406C"));
                //                             color = Colors.grey;
                //                             textcolor1 = Colors.black;
                //                             textcolor2 = Colors.white;
                //                           });
                //                         },
                //                         child: Container(
                //                             decoration: BoxDecoration(
                //                               color: color1,
                //                             ),
                //                             child: Center(
                //                               child: Padding(
                //                                 padding:
                //                                     const EdgeInsets.all(8.0),
                //                                 child: Text(
                //                                   "Hindi",
                //                                   style: TextStyle(
                //                                       color: textcolor1),
                //                                 ),
                //                               ),
                //                             )),
                //                       )
                //                     ],
                //                   )),
                //             ),
                //           )
                //         ],
                //       ),
                //     )
                //   ],
                // ),
                SizedBox(
                  height: MediaQuery.of(context).size.width * 0.2,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.6,
                  child: _subchpterListview(context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }

  void _getSubChapterList(BuildContext context) async {
    showLoaderDialog(context);
    Map map = {"ChapterID": chapterId};

    String response =
        await Webapi().postWithbody(ApiUrl.Get_Sub_Chapter_of_Book, map);

    setState(() {
      Navigator.of(context).pop();
      var jsondata = json.decode(response);
      var message = jsondata['Message'];
      if (message.toString() == "Success") {
        debugPrint("AP DATA $response");
        PreferenceUtils.setUserPojo("_getSubChapterList", response);
        _subchapterOfBookPojo = getSubChapterPojoFromJson(response);
        _subchapterofBookliist = _subchapterOfBookPojo.data;

        for (int i = 0; i < _subchapterofBookliist.length; i++) {
          saveBookChapter(
              _subchapterofBookliist[i].id,
              _subchapterofBookliist[i].chapterId,
              _subchapterofBookliist[i].chapterNameEnglish == null
                  ? ""
                  : _subchapterofBookliist[i].chapterNameEnglish,
              _subchapterofBookliist[i].chapterNameHindi == null
                  ? ""
                  : _subchapterofBookliist[i].chapterNameHindi,
              _subchapterofBookliist[i].chapterTextEnglish == null
                  ? ""
                  : _subchapterofBookliist[i].chapterTextEnglish,
              _subchapterofBookliist[i].chapterTextHindi == null
                  ? ""
                  : _subchapterofBookliist[i].chapterTextHindi,
              _subchapterofBookliist[i].chapterMeaningTextEnglish == null
                  ? ""
                  : _subchapterofBookliist[i].chapterMeaningTextEnglish,
              _subchapterofBookliist[i].chapterMeaningTextHindi == null
                  ? ""
                  : _subchapterofBookliist[i].chapterMeaningTextHindi,
              _subchapterofBookliist[i].chapterAudioPathEnglish == null
                  ? ""
                  : _subchapterofBookliist[i].chapterAudioPathEnglish,
              _subchapterofBookliist[i].chapterAudioPathEnglish == null
                  ? ""
                  : _subchapterofBookliist[i].chapterAudioPathEnglish);
        }
      } else {
        debugPrint("ERROR ERROR $message");
      }
    });
  }

  Widget _subchpterListview(context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          itemCount: _subchapterofBookliist.length,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemBuilder: (context, position) {
            return Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.09,
                  right: MediaQuery.of(context).size.width * 0.09,
                  bottom: MediaQuery.of(context).size.width * 0.02),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SubChapterWithMeaniing(
                            _subchapterofBookliist[position]),
                      ));
                  /*todo---yaha pe check krna hai ki user to practice se aaya hai ya to perform se
                    *  agr to practice se aaya hai to With meaning wale nhi to WIthout Meaning wale pe*/
                },
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.1 - 22,
                  child: Text(
                    hindiCode == "0"
                        ? _subchapterofBookliist[position].chapterNameEnglish +
                            ""
                        : _subchapterofBookliist[position].chapterNameHindi !=
                                null
                            ? _subchapterofBookliist[position].chapterNameHindi
                            : "N/A",
                    style: TextStyle(
                        fontFamily: "Roboto Regular",
                        fontSize: MediaQuery.of(context).size.width * 0.05,
                        color: Colors.white),
                  ),
                  decoration: Commonmethod.decoration(),
                ),
              ),
            );
          }),
    );
  }

  void saveBookChapter(
      int subchapterid,
      int subbookid,
      String subchapterEnglishname,
      String subchapternameHindi,
      String subchaptetTextenglish,
      String subchapterTextHindi,
      String subchapterMeaningEnglish,
      String subchapterMeaningHindi,
      String subchapterAudiopathEng,
      String subchapterAudioHindi) async {
    Map<String, dynamic> row = {
      DataBaseHelper.SUB_CHAPTER_LIST_ID: subchapterid,
      DataBaseHelper.SUB_CHAPTER_BOOK_ID: subbookid,
      DataBaseHelper.SUB_CHAPTER_LIST_ChapterNameEnglish: subchapterEnglishname,
      DataBaseHelper.SUB_CHAPTER_LIST_ChapterNameHindi: subchapternameHindi,
      DataBaseHelper.SUB_CHAPTER_LIST_ChapterTextEnglish: subchaptetTextenglish,
      DataBaseHelper.SUB_CHAPTER_LIST_ChapterTextHindi: subchapterTextHindi,
      DataBaseHelper.SUB_CHAPTER_LIST_ChapterMeaningTextEnglish:
          subchapterMeaningEnglish,
      DataBaseHelper.SUB_CHAPTER_LIST_ChapterMeaningTextHindi:
          subchapterMeaningHindi,
      DataBaseHelper.SUB_CHAPTER_LIST_ChapterAudioPathEnglish:
          subchapterAudiopathEng,
      DataBaseHelper.SUB_CHAPTER_LIST_ChapterChapterAudioPathHindi:
          subchapterAudioHindi
    };
    var id = await dbhelper.insert(DataBaseHelper.table_SUB_CHAPTER_LIST, row);
    debugPrint("SUB CHAPTER  SAVE $id");
  }

  void getAllSubChapter(bookid) async {
    var response = await dbhelper.getBookSubChapter(int.parse(bookid));
    setState(() {
      _subchapterofBookliist = response;

      debugPrint(_subchapterofBookliist.length.toString() + "   sdklfsdfsdf");
    });
  }

}
