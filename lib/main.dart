import 'package:flutter/material.dart';
import 'package:flutter_app/homepage/HomePage.dart';
import 'package:flutter_app/splash/SplashPage.dart';

import 'downloadpractice.dart';

void main() {
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(debugShowCheckedModeBanner: false, home: SplashPage());
  }
}
