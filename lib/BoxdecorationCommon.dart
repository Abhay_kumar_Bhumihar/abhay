import 'package:flutter/material.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:flutter_svg/svg.dart';

class Commonmethod {
  static String chaptertext =
      "Lorem ipsum dolor sit amet, cu cum dico theophrastus, mel fierent probatus contentiones ad. No sea vidit utinam mediocritatem, ex vis fierent propriae principes, insolens phaedrum ocurreret ei vim. Magna detracto pertinacia sed ei, ridens vocent tritani eam cu. Mea ancillae expetendis dissentiunt ei. Nostrum expetenda an mel. Pro ex voluptua principes assueverit.";

  static BoxDecoration decoration() {
    return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: Color(hexStringToHexInt("#6C6C86")),
        border: Border.all(color: Color(hexStringToHexInt("#5C6D8F"))));
  }

  static Widget logowithText(Color color, BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: MediaQuery.of(context).size.width * 0.1,
        ),
        Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.2,
            child: SvgPicture.asset(
              "assets/logo.svg",
            )),
        Padding(
          padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.1,
              right: MediaQuery.of(context).size.width * 0.1,
              top: MediaQuery.of(context).size.width * 0.04),
          child: Text(
            "Jinvani",
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.1 + 15,
                color: PreferenceUtils.getString("themeColor").toString() == "0"
                    ? Colors.white
                    : Color(hexStringToHexInt("#0B0B38")),
                fontFamily: "Poorich"),
          ),
        ),
      ],
    );
  }
  static Widget logowithTextformediaplayer(Color color, BuildContext context) {
    return Column(
      children: <Widget>[

        Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.2,
            child: SvgPicture.asset(
              "assets/logo.svg",
            )),
        Padding(
          padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.1,
              right: MediaQuery.of(context).size.width * 0.1,
              ),
          child: Text(
            "Jinvani",
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.1 + 15,
                color: PreferenceUtils.getString("themeColor").toString() == "0"
                    ? Colors.white
                    : Color(hexStringToHexInt("#0B0B38")),
                fontFamily: "Poorich"),
          ),
        ),
      ],
    );
  }

  static BoxDecoration backgrounDecoration(colorCode) {
    return BoxDecoration(
      image: DecorationImage(
          image: AssetImage(backgroundImage(colorCode)), fit: BoxFit.fill),
    );
  }

  //0--->black
  //1-->white

  static setData() {
    var shareval = PreferenceUtils.getString("themeColor").toString();
    print(shareval);
    if (shareval == "0") {
      PreferenceUtils.setString("themeColor", "1");
      debugPrint("111111111111");
    } else if (shareval == "1") {
      PreferenceUtils.setString("themeColor", "0");
      debugPrint("OOOOOOOOOO");
    }
  }

  static String backgroundImage(String themeColor) {
    //var shareval =   PreferenceUtils.getString("themeColor") ;
    if (themeColor== "0") {
      return "assets/blue_background.png";
    } else {
      return "assets/white_background.png";
    }
  }

  static hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }
}
