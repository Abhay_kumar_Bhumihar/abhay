import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';

import 'BoxdecorationCommon.dart';

class MalaPge extends StatefulWidget {
  @override
  _MalaPgeState createState() => _MalaPgeState();
}

class _MalaPgeState extends State<MalaPge> {
  var colorCode = "0";
  int countMala = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    PreferenceUtils.init();
    setState(() {
      colorCode = PreferenceUtils.getString("themeColor").toString();
    });
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarColor: Colors.white,
    // ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: Color(hexStringToHexInt("#6C6C88")),
        centerTitle: true,
        title: Text(
          "Jinvani",
          style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.12,
              color: Colors.white,
              fontFamily: "Poorich"),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Commonmethod.backgroundImage(colorCode)),
              fit: BoxFit.fill),
        ),
        child: Column(
          children: <Widget>[
            Expanded(
                child: Align(
              alignment: FractionalOffset.center,
              child: Container(
                child: Text(
                  countMala.toString(),
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )),
            Expanded(
              child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      MaterialButton(
                        color: Colors.red,
                        onPressed: () {
                          setState(() {
                            countMala = countMala + 1;
                          });
                        },
                        child: Text('TAP HERE...'),
                      ),
                      MaterialButton(
                        color: Colors.red,
                        onPressed: () {
                          setState(() {
                            countMala = 0;
                          });
                        },
                        child: Text('Reset...'),
                      ),
                    ],
                  )),
            ),
          ],
        ),
      ),
    ));
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }
}
