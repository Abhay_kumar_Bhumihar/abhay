// To parse this JSON data, do
//
//     final audioFilePojo = audioFilePojoFromJson(jsonString);

import 'dart:convert';

AudioFilePojo audioFilePojoFromJson(String str) =>
    AudioFilePojo.fromJson(json.decode(str));

String audioFilePojoToJson(AudioFilePojo data) => json.encode(data.toJson());

class AudioFilePojo {
  AudioFilePojo({
    this.message,
    this.data,
    this.userId,
  });

  String message;
  List<AudioResponse> data;
  int userId;

  factory AudioFilePojo.fromJson(Map<String, dynamic> json) => AudioFilePojo(
        message: json["Message"],
        data: List<AudioResponse>.from(
            json["Data"].map((x) => AudioResponse.fromJson(x))),
        userId: json["userId"],
      );

  Map<String, dynamic> toJson() => {
        "Message": message,
        "Data": List<dynamic>.from(data.map((x) => x.toJson())),
        "userId": userId,
      };
}

class AudioResponse {
  AudioResponse({
    this.id,
    this.subAudioId,
    this.audioFilePath,
  });

  int id;
  int subAudioId;
  String audioFilePath;

  factory AudioResponse.fromJson(Map<String, dynamic> json) => AudioResponse(
        id: json["ID"],
        subAudioId: json["SubAudioID"],
        audioFilePath: json["AudioFilePath"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "SubAudioID": subAudioId,
        "AudioFilePath": audioFilePath,
      };
}
