import 'dart:convert';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/homepage/GetSubAudioTitle.dart';
import 'package:flutter_app/networkRequest/ApiUrl.dart';
import 'package:flutter_app/networkRequest/Webapi.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:marquee/marquee.dart';

import '../BoxdecorationCommon.dart';
import '../NewAdudioPractice.dart';
import 'AudioFilePojo.dart';

class PlayAudioPage extends StatefulWidget {
  String audioId;

  PlayAudioPage(String audioId) {
    this.audioId = audioId;
  }

  @override
  _PlayAudioPageState createState() => _PlayAudioPageState(audioId);
}

class _PlayAudioPageState extends State<PlayAudioPage> {
  String audioId;
  var TAG = "_PlayAudioPageState ";

  _PlayAudioPageState(String audioId) {
    this.audioId = audioId;
  }

  int _addedCount = 0;
  AudioFilePojo audioFilePojo;
  List<AudioResponse> audioResponseList = [];
  var colorCode = "0";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    PreferenceUtils.init();
    setState(() {
      colorCode = PreferenceUtils.getString("themeColor").toString();
    });
    _getSubAudio();

    debugPrint(
        "$TAG PreferenceUtils themeColor=${PreferenceUtils.getString("themeColor").toString()}");
  }

//SubAudioID=1

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: Color(hexStringToHexInt("#6C6C88")),
        centerTitle: true,
        title: Text(
          "Jinvani",
          style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.12,
              color: Colors.white,
              fontFamily: "Poorich"),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Commonmethod.backgroundImage(colorCode)),
              fit: BoxFit.fill),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.1,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.2,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: _mainAudioListview(context),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  void _getSubAudio() async {
    //  showLoaderDialog(context);
    Map map = {"SubAudioID": audioId};
    String response =
        await Webapi().postWithbody(ApiUrl.Get_all_Audio_Files, map);

    setState(() {
      //   Navigator.of(context).pop();
      var jsondata = json.decode(response);

      var message = jsondata['Message'];
      if (message.toString() == "Success") {
        debugPrint("AP DATA $response");
        audioFilePojo = audioFilePojoFromJson(response);
        audioResponseList = audioFilePojo.data;
      } else {
        debugPrint("ERROR ERROR $message");
      }
    });
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }

  Widget _mainAudioListview(context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          itemCount: audioResponseList.length,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemBuilder: (context, position) {
            return Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.09,
                  right: MediaQuery.of(context).size.width * 0.09,
                  bottom: MediaQuery.of(context).size.width * 0.02),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AudioApp(
                            audioResponseList[position]
                                .audioFilePath
                                .toString())),
                  );
                },
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.1 - 22,
                  child: Marquee(
                    text: audioResponseList[position]
                            .audioFilePath
                            .substring(19) +
                        "",
                    style: TextStyle(
                        fontFamily: "Roboto Regular",
                        fontSize: MediaQuery.of(context).size.width * 0.05,
                        color: Colors.white),
                    scrollAxis: Axis.horizontal,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    blankSpace: 20.0,
                    velocity: 20.0,
                    pauseAfterRound: Duration(seconds: 1),
                    startPadding: 10.0,
                    accelerationDuration: Duration(seconds: 1),
                    accelerationCurve: Curves.linear,
                    decelerationDuration: Duration(milliseconds: 50),
                    decelerationCurve: Curves.easeOut,
                  ),
                  decoration: Commonmethod.decoration(),
                ),
              ),
            );
          }),
    );
  }
}
