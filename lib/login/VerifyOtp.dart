import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/homepage/HomePage.dart';
import 'package:flutter_app/login/GetOptPojo.dart';
import 'package:flutter_app/networkRequest/ApiUrl.dart';
import 'package:flutter_app/networkRequest/Webapi.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../BoxdecorationCommon.dart';

class VerifyOtp extends StatefulWidget {
  var mobno = "";

  VerifyOtp(String mobno) {
    this.mobno = mobno;
  }

  @override
  _VerifyOtpState createState() => _VerifyOtpState(mobno);
}

class _VerifyOtpState extends State<VerifyOtp> {
  var mobno = "";
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  var TAG = "_VerifyOtpState ";
  GetOptPojo _loginPageResponse;
  var colorCode = "0";

  _VerifyOtpState(String mobno) {
    this.mobno = mobno;
  }

  TextEditingController textEditingController = TextEditingController();

  StreamController<ErrorAnimationType> errorController;

  bool hasError = false;
  String currentText = "";
  var otp = "";

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
    setState(() {
      colorCode = PreferenceUtils.getString("themeColor").toString();
      print("$TAG PreferenceUtils=" + PreferenceUtils.getString("themeColor"));
    });
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarColor: Colors.white,
    // ));
  }

  @override
  void dispose() {
    errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      key: _scaffoldkey,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              //   image: AssetImage(Commonmethod.backgrounDecoration(colorCode) ),
              image: AssetImage(Commonmethod.backgroundImage(colorCode)),
              fit: BoxFit.fill),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.1,
              ),
              Commonmethod.logowithText(Colors.black, context),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.03,
                    left: MediaQuery.of(context).size.width * 0.1),
                child: Text(
                  "Enter OTP",
                  style: TextStyle(
                      color: PreferenceUtils.getString("themeColor") == "0"
                          ? Colors.white
                          : Color(hexStringToHexInt("#0A0A37")),
                      fontSize: MediaQuery.of(context).size.width * 0.04,
                      fontFamily: "Roboto Light"),
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.01,
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.02,
                    left: MediaQuery.of(context).size.width * 0.1,
                    right: MediaQuery.of(context).size.width * 0.1),
                child: PinCodeTextField(
                  appContext: context,
                  pastedTextStyle: TextStyle(
                    color: Colors.green.shade600,
                    fontWeight: FontWeight.bold,
                  ),
                  length: 6,
                  obscureText: true,
                  obscuringCharacter: '*',
                  /*obscuringWidget: FlutterLogo(
                    size: 24,
                  ),*/
                  blinkWhenObscuring: true,
                  animationType: AnimationType.fade,
                  pinTheme: PinTheme(
                    inactiveColor:
                        PreferenceUtils.getString("themeColor") == "0"
                            ? Colors.white
                            : Color(hexStringToHexInt("#6C6C86")),
                    selectedColor:
                        PreferenceUtils.getString("themeColor") == "0"
                            ? Colors.white
                            : Color(hexStringToHexInt("#6C6C86")),
                    activeColor: PreferenceUtils.getString("themeColor") == "0"
                        ? Colors.white
                        : Color(hexStringToHexInt("#6C6C86")),
                    selectedFillColor:
                        PreferenceUtils.getString("themeColor") == "0"
                            ? Colors.white
                            : Color(hexStringToHexInt("#6C6C86")),
                    inactiveFillColor:
                        PreferenceUtils.getString("themeColor") == "0"
                            ? Colors.white
                            : Color(hexStringToHexInt("#6C6C86")),
                    shape: PinCodeFieldShape.box,
                    borderRadius: BorderRadius.circular(5),
                    fieldHeight: 40,
                    fieldWidth: 40,
                    activeFillColor: Colors.white,
                  ),
                  cursorColor: Colors.black,
                  animationDuration: Duration(milliseconds: 300),
                  enableActiveFill: true,
                  errorAnimationController: errorController,
                  controller: textEditingController,
                  keyboardType: TextInputType.number,
                  boxShadows: [
                    BoxShadow(
                      offset: Offset(0, 1),
                      color: Colors.black12,
                      blurRadius: 10,
                    )
                  ],
                  onCompleted: (v) {
                    otp = v;
                    print("Completed");
                  },
                  // onTap: () {
                  //   print("Pressed");
                  // },
                  onChanged: (value) {
                    print(value);
                    setState(() {
                      // currentText = value;
                    });
                  },
                  beforeTextPaste: (text) {
                    print("Allowing to paste $text");
                    //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                    //but you can show anything you want here, like your pop up saying wrong paste format or etc
                    return true;
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.3,
                    left: MediaQuery.of(context).size.width * 0.1 - 15,
                    right: MediaQuery.of(context).size.width * 0.1 - 15),
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage()),
                      );
                    },
                    child: Container(
                      height: 45.0,
                      decoration: BoxDecoration(
                        color: Color(hexStringToHexInt("#080B38")),
                        borderRadius: BorderRadius.all(Radius.circular(25.0)),
                      ),
                      child: Center(
                        child: Text('Get Started',
                            style: new TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Roboto Regular',
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }

  getTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String stringValue = prefs.getString('theme');
    return stringValue;
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }

  String getBackgroundAssetName() {
    if (getTheme() != null) {
      if (getTheme() == "0") return "assets/blue_background.png";
      if (getTheme() == "1") return "assets/white_background.png";
    }
    return 'assets/blue_background.png';
  }

  void veryotp(BuildContext context) async {
    showLoaderDialog(context);
    // /
    // Mmobile=12345&otp=123456ap<String, String> get queryParameters;
    var map = {"mobile": mobno, "otp": otp};
    debugPrint(map.toString());
    String response = await Webapi().postWithbody(ApiUrl.LOGIN, map);
    print(response);
    Navigator.of(context).pop();
    _loginPageResponse = getOptPojoFromJson(response);
    if (_loginPageResponse.message == "Success") {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => HomePage()),
      );
    }
  }

  void showSnackBar(String message) {
    final snackBarContent = SnackBar(
      content: Text(
        message,
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18.0),
      ),
      action: SnackBarAction(
          label: 'OK',
          onPressed: _scaffoldkey.currentState.hideCurrentSnackBar),
      duration: Duration(seconds: 5),
    );
    _scaffoldkey.currentState.showSnackBar(snackBarContent);
  }
}
