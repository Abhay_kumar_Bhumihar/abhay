// To parse this JSON data, do
//
//     final getOptPojo = getOptPojoFromJson(jsonString);

import 'dart:convert';

GetOptPojo getOptPojoFromJson(String str) =>
    GetOptPojo.fromJson(json.decode(str));

String getOptPojoToJson(GetOptPojo data) => json.encode(data.toJson());

class GetOptPojo {
  GetOptPojo({
    this.message,
    this.data,
    this.userId,
  });

  String message;
  String data;
  int userId;

  factory GetOptPojo.fromJson(Map<String, dynamic> json) => GetOptPojo(
        message: json["Message"],
        data: json["Data"],
        userId: json["userId"],
      );

  Map<String, dynamic> toJson() => {
        "Message": message,
        "Data": data,
        "userId": userId,
      };
}
