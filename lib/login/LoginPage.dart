import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/homepage/HomePage.dart';
import 'package:flutter_app/login/GetOptPojo.dart';
import 'package:flutter_app/login/VerifyOtp.dart';
import 'package:flutter_app/networkRequest/ApiUrl.dart';
import 'package:flutter_app/networkRequest/Webapi.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../BoxdecorationCommon.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GetOptPojo _loginPageResponse;
  TextEditingController _mobileocontroller = TextEditingController();
  var colorCode = "0";
  var TAG = "_LoginPageState ";
  bool _isLoggedIn = false;


  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);

  _signIn(BuildContext context) async {
    await _logoutGoogle();

    try {
      await _googleSignIn.signIn();
      setState(() {

        debugPrint(
            "SDF SDF SDF ${_googleSignIn.currentUser.id}, ${_googleSignIn.currentUser.email}, ${_googleSignIn.currentUser.displayName}");
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
        );
      });
    } catch (err) {
      print(err);
    }
  }

  _logoutGoogle() {
    _googleSignIn.signOut();
    setState(() {

      _isLoggedIn = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    PreferenceUtils.init();

    if (PreferenceUtils.getString("themeColor") == null ||
        PreferenceUtils.getString("themeColor") == "") {
      PreferenceUtils.setString("themeColor", "0");
    }
    setState(() {
      colorCode = PreferenceUtils.getString("themeColor").toString();
      print("$TAG PreferenceUtils=" + PreferenceUtils.getString("themeColor"));
    });
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarColor: Colors.white,
    // ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Commonmethod.backgroundImage(colorCode)),
              fit: BoxFit.fill),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Commonmethod.logowithText(Colors.black, context),
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.3,
              ),
              // Padding(
              //   padding: EdgeInsets.only(
              //       left: MediaQuery.of(context).size.width * 0.1),
              //   child: Text(
              //     "Mobile Number",
              //     style: TextStyle(
              //         color: PreferenceUtils.getString("themeColor") == "0"
              //             ? Colors.white
              //             : Colors.black,
              //         fontSize: MediaQuery.of(context).size.width * 0.04,
              //         fontFamily: "Roboto Light"),
              //     textAlign: TextAlign.left,
              //   ),
              // ),
              // SizedBox(
              //   height: MediaQuery.of(context).size.width * 0.02,
              // ),
              // Padding(
              //     padding: EdgeInsets.only(
              //         left: MediaQuery.of(context).size.width * 0.1,
              //         right: MediaQuery.of(context).size.width * 0.1),
              //     child: Container(
              //       color: PreferenceUtils.getString("themeColor") == "0"
              //           ? Colors.white
              //           : Color(hexStringToHexInt("#6C6C86")),
              //       height: 50.0,
              //       child: TextField(
              //         controller: _mobileocontroller,
              //         style: TextStyle(
              //             color: PreferenceUtils.getString("themeColor") == "0"
              //                 ? Color(hexStringToHexInt("#2C406C"))
              //                 : Colors.white,
              //             fontFamily: "Roboto Light"),
              //         decoration: InputDecoration(
              //           hintText: " Enter mobile no",
              //           hintStyle: TextStyle(
              //               color:
              //                   PreferenceUtils.getString("themeColor") == "0"
              //                       ? Color(hexStringToHexInt("#2C406C"))
              //                       : Colors.white,
              //               fontFamily: "Roboto Light"),
              //           border: OutlineInputBorder(
              //             borderSide: BorderSide(color: Colors.white),
              //             borderRadius: BorderRadius.circular(0.0),
              //           ),
              //           enabledBorder: UnderlineInputBorder(
              //             borderSide: BorderSide(color: Colors.white),
              //             borderRadius: BorderRadius.circular(1.0),
              //           ),
              //         ),
              //       ),
              //     )),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width * 0.1,
                    left: MediaQuery.of(context).size.width * 0.1 - 15,
                    right: MediaQuery.of(context).size.width * 0.1 - 15),
                child: Center(
                  child: GestureDetector(
                    onTap: () {
                      _signIn(context);

                    },
                    child: Container(
                      height: 45.0,
                      decoration: BoxDecoration(
                        color: Color(hexStringToHexInt("#080B38")),
                        borderRadius: BorderRadius.all(Radius.circular(25.0)),
                      ),
                      child: Center(
                        child: Text('Login With Google',
                            style: new TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Roboto Regular',
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }

  void sendOtp(BuildContext context) async {
    showLoaderDialog(context);
    // Map<String, String> get queryParameters;
    var map = {"mobile": _mobileocontroller.text.toString()};
    debugPrint(map.toString());
    String response = await Webapi().postWithbody(ApiUrl.GET_OTO, map);
    print(response);
    Navigator.of(context).pop();
    _loginPageResponse = getOptPojoFromJson(response);
    if (_loginPageResponse.message == "Success") {}
  }
}
