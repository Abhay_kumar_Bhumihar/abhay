import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/homepage/HomePage.dart';
import 'package:flutter_app/login/LoginPage.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:flutter_svg/svg.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../BoxdecorationCommon.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  var choosedThemeColor = Colors.white;
  var TAG = "_SplashPageState ";
  var colorCode = "0";
   Permission _permission;
  PermissionStatus _permissionStatus = PermissionStatus.denied;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkSharedValues();
    _listenForPermissionStatus();
    Permission permission;
    permission=Permission.storage;
    requestPermission(permission);
  }

  void _listenForPermissionStatus() async {
    final status = await _permission.status;
    setState(() => _permissionStatus = status);
  }

  Future<void> requestPermission(Permission permission) async {
    final status = await permission.request();

    setState(() {
      print(status);
      _permissionStatus = status;
      print(_permissionStatus);
    });
  }
  void navigateUser() async {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(
                  (Commonmethod.backgroundImage(colorCode) != null &&
                          Commonmethod.backgroundImage(colorCode) != "")
                      ? Commonmethod.backgroundImage(colorCode)
                      : 'assets/blue_background.png'),
              fit: BoxFit.fill),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.08,
              ),
              Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.3,
                  child: SvgPicture.asset(
                    "assets/logo.svg",
                  )),
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.06,
              ),
              Text(
                "Jinvani",
                style: TextStyle(
                    fontSize: MediaQuery.of(context).size.width * 0.1,
                    color:
                        /*( choosedThemeColor!=null && choosedThemeColor== "0")
                        ? Colors.white
                        : Color(Commonmethod.hexStringToHexInt("#0B0B38"))*/
                        choosedThemeColor,
                    fontFamily: "Poorich"),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.3,
              ),
              Text(
                "LOADING",
                style: TextStyle(
                    letterSpacing: 2,
                    fontSize: 20,
                    color:
                        /* PreferenceUtils.getString("themeColor") == "0"
                        ? Colors.white
                        : Colors.black,*/
                        choosedThemeColor,
                    fontFamily: "Roboto Light"),
              ),
              Text(
                "Please wait..",
                style: TextStyle(
                    fontSize: 20,
                    color:
                        /*PreferenceUtils.getString("themeColor") == "0"
                        ? Colors.white
                        : Colors.black*/
                        choosedThemeColor,
                    fontFamily: "Roboto Regular"),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  Future<void> checkSharedValues() async {
    await PreferenceUtils.init();

    setState(() {
      if (PreferenceUtils.getString("hindicode").toString() == null ||
          PreferenceUtils.getString("hindicode").toString() == "") {
        PreferenceUtils.setString("hindicode", "0");
      }

      try {
        debugPrint(
            "$TAG tryBlock  ${PreferenceUtils.getString("themeColor").toString()}");
        if (PreferenceUtils.getString("themeColor").toString() == null ||
            PreferenceUtils.getString("themeColor").toString() == "") {
          debugPrint("if block 1");
          PreferenceUtils.setString("themeColor", "0");
          choosedThemeColor = Colors.white;
          colorCode = "0";
        } else {
          debugPrint("else block 1");
          if (PreferenceUtils.getString("themeColor").toString() == "0") {
            debugPrint("else block 1.1");
            choosedThemeColor = Colors.white;
            colorCode = "0";
          } else if (PreferenceUtils.getString("themeColor").toString() ==
              "1") {
            choosedThemeColor = Colors.black;
            colorCode = "1";
          } else {
            PreferenceUtils.setString("themeColor", "0");
            choosedThemeColor = Colors.white;
            colorCode = "0";
          }
        }
      } catch (e) {
        PreferenceUtils.setString("themeColor", "0");
        choosedThemeColor = Colors.white;
        colorCode = "0";
      }
    });

    new Future.delayed(const Duration(seconds: 5), () => navigateUser());
  }
}
