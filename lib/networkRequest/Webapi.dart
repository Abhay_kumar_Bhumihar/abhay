import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

import 'ApiUrl.dart';


class Webapi {
  final String BASEUR = "https://jsonplaceholder.typicode.com/albums";

  /*todo----Login api*/

  Future<String> postWithbody(String url, Map map) async {
    final response =
        await http.post(Uri.parse(ApiUrl.BASEURL + url), body: map);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      debugPrint(response.body + "  dfdfdf");
      return response.body;
    }
  }

  Future<String> postWithoutbody(String url) async {
    final response = await http.post(
      Uri.parse(ApiUrl.BASEURL + url),
    );
    debugPrint("ABHAY KUMAR $response");
    if (response.statusCode == 200) {
      return response.body;
    } else {
      debugPrint("sdfsdfsdfsdfsdfdsfdsfdsf ${response.body}" + "  dfdfdf");
      return response.body;
    }
  }

  Future<bool> check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  Future<bool> isInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network, make sure there is actually a net connection.
      if (await DataConnectionChecker().hasConnection) {
        // Mobile data detected & internet connection confirmed.
        return true;
      } else {
        // Mobile data detected but no internet connection found.
        return false;
      }
    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a WIFI network, make sure there is actually a net connection.
      if (await DataConnectionChecker().hasConnection) {
        // Wifi detected & internet connection confirmed.
        return true;
      } else {
        // Wifi detected but no internet connection found.
        return false;
      }
    } else {
      // Neither mobile data or WIFI detected, not internet connection found.
      return false;
    }
  }
}
