import 'package:shared_preferences/shared_preferences.dart';

class ApiUrl {
/*todo------api url------*/
  static final BASEURL = "http://ssjitservices.info/api/";
  static final GET_OTO = "LoginApi/GenerateOtp";
  static final LOGIN = "LoginApi/userlogin";
  static final Get_Main_Audios_Title = "ValuesApi/GetMainAudios";
  static final Get_Sub_Audio_Titles = "ValuesApi/GetSubAudios";
  static final Get_all_Audio_Files = "ValuesApi/GetAudioFiles";


  static final Get_Main_Book_List = "ValuesApi/GetBooks";
  static final Get_Chapter_ofBook = "ValuesApi/GetChaptersofbook";
  static final Get_Sub_Chapter_of_Book = "ValuesApi/GetSubChaptersofbook";
}
