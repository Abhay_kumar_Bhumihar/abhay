import 'dart:async' show Future;
import 'package:flutter_app/ChapterListFile/GetChapterOfBookPojo.dart';
import 'package:flutter_app/ChapterListFile/GetSubChapterPojo.dart';
import 'package:flutter_app/homepage/GetMainAudioTitle.dart';
import 'package:flutter_app/homepage/GetMainBookList.dart';
import 'package:flutter_app/homepage/GetSubAudioTitle.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenceUtils {
  static Future<SharedPreferences> get _instance async =>
      _prefsInstance ??= await SharedPreferences.getInstance();
  static SharedPreferences _prefsInstance;

  // call this method from iniState() function of mainApp().
  static Future<SharedPreferences> init() async {
    _prefsInstance = await _instance;
    return _prefsInstance;
  }

  /*todo----it is for get data with key*/
  static String  getString(String key, [String defValue])  {
    return  _prefsInstance.getString(key) ?? defValue ?? "0";
  }

  /*todo----it is for set data in shared with key*/
  static Future<bool> setString(String key, String value) async {
    var prefs = await _instance;
    return prefs?.setString(key, value) ?? Future.value(false);
  }

  /*todo----it is use for get set user data in pojo*/
  static Future<bool> setUserPojo(String key, String value) async {
    var prefs = await _instance;
    return prefs?.setString(key, value) ?? Future.value(false);
  }

/*todo----it is use for get set user data in pojo*/

  /*todo---it is use for get Main Audi list title*/
  static GetMainAudioTitle getUserdata(String key) {
    return getMainAudioTitleFromJson(_prefsInstance.getString(key));
  }

  /*todo----it is use for get Sub Audio list title*/
  static GetSubAudioTitle getSubAudioTitle(String key) {
    return getSubAudioTitleFromJson(_prefsInstance.get(key));
  }

  /*todo---it is use for get Main Book Title list*/

  static GetMainBookList getMainBookList(String key) {
    return getMainBookListFromJson(_prefsInstance.get(key));
  }

/*todo----it is use for get all Chapter of , main book list item*/
  static GetChapterOfBookPojo getChapterList(String key) {
    return getChapterOfBookPojoFromJson(_prefsInstance.get(key));
  }

  /*todo-----it is use if chapter has subchapter , then it will give sub chapter list*/

  static GetSubChapterPojo getSubChapterList(key) {
    return getSubChapterPojoFromJson(_prefsInstance.getString(key));
  }
}
