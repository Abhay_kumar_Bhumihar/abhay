import 'package:flutter_app/ChapterListFile/GetChapterOfBookPojo.dart';
import 'package:flutter_app/ChapterListFile/GetSubChapterPojo.dart';
import 'package:flutter_app/homepage/GetMainAudioTitle.dart';
import 'package:flutter_app/homepage/GetMainBookList.dart';
import 'package:flutter_app/homepage/GetSubAudioTitle.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:io';
import 'package:sqflite/sqflite.dart';

class DataBaseHelper {
  static final _databasename = "richestfot";
  static final _dtatabaseversion = 1;

  /*todo----main audio list*/
  static final tableMAINAUDIO = "mainAudio";
  static final MAIN_AUDIOID = 'ID';
  static final MAIN_AUDIO_ENGLISHNAME = "main_AudioNameEnglish";
  static final MAIN_AUDIO_HINDINAME = "main_AudioNameHindi";

  /*todo-----sub audio list page*/

  static final table_SUB_AUDIO = "subAudio";
  static final SUB__AUDIOID = 'ID_SUBAUDIO';
  static final SUB_MAIN_AUDIOID = "sub_mainaudio_id";
  static final SUB_AUDIO_ENGLISHNAME = "sub_AudioNameEnglish";
  static final SUB_AUDIO_HINDINAME = "sub_AudioNameHindi";

  /*todo-----main book list page*/

  static final table_MAIN_Book = "mainBook";
  static final MAIN_BOOK_ID = 'ID';
  static final MAIN_BOOK_ENGLISHNAME = "main_BookNameEnglish";
  static final MAIN_BOOK_HINDINAME = "main_BookNameHindi";

  /*todo-------Chapter list page*/
  static final table_CHAPTER_LIST = "chapterlist";
  static final CHAPTER_LIST_ID = "chater_listID";
  static final CHAPTER_BOOK_ID = "chapter_bookId";
  static final CHAPTER_LIST_ChapterNameEnglish = "chapter_ChapterNameEnglish";
  static final CHAPTER_LIST_ChapterNameHindi = "chapter_ChapterNameHindi";
  static final CHAPTER_LIST_ChapterTextEnglish = "chapter_ChapterTextEnglish";
  static final CHAPTER_LIST_ChapterTextHindi = "chapter_ChapterTextHindi";
  static final CHAPTER_LIST_ChapterMeaningTextEnglish =
      "chapter_ChapterMeaningTextEnglish";
  static final CHAPTER_LIST_ChapterMeaningTextHindi =
      "chapter_ChapterMeaningTextHindi";
  static final CHAPTER_LIST_ChapterAudioPathEnglish =
      "chapter_ChapterAudioPathEnglish";
  static final CHAPTER_LIST_ChapterChapterAudioPathHindi =
      "chapter_ChapterAudioPathHindi";
  static final CHAPTER_LIST_ChapterIsSubChapter = "IsSubChapter";

  /*todo--------SUb chapter list page*/
  static final table_SUB_CHAPTER_LIST = "sub_chapterlist";
  static final SUB_CHAPTER_LIST_ID = "sub_chater_listID";
  static final SUB_CHAPTER_BOOK_ID = "sub_chapter_bookId";
  static final SUB_CHAPTER_LIST_ChapterNameEnglish =
      "sub_chapter_ChapterNameEnglish";
  static final SUB_CHAPTER_LIST_ChapterNameHindi =
      "sub_chapter_ChapterNameHindi";
  static final SUB_CHAPTER_LIST_ChapterTextEnglish =
      "sub_chapter_ChapterTextEnglish";
  static final SUB_CHAPTER_LIST_ChapterTextHindi =
      "sub_chapter_ChapterTextHindi";
  static final SUB_CHAPTER_LIST_ChapterMeaningTextEnglish =
      "sub_chapter_ChapterMeaningTextEnglish";
  static final SUB_CHAPTER_LIST_ChapterMeaningTextHindi =
      "sub_chapter_ChapterMeaningTextHindi";
  static final SUB_CHAPTER_LIST_ChapterAudioPathEnglish =
      "sub_chapter_ChapterAudioPathEnglish";
  static final SUB_CHAPTER_LIST_ChapterChapterAudioPathHindi =
      "sub_chapter_ChapterAudioPathHindi";

  DataBaseHelper._privateContructor();

  static final DataBaseHelper instance = DataBaseHelper._privateContructor();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, _databasename);
    return await openDatabase(path,
        version: _dtatabaseversion, onCreate: _oncreate);
  }

  Future _oncreate(Database db, int version) async {
    await db.execute('''
      create table if not exists $tableMAINAUDIO (
        $MAIN_AUDIOID integer primary key ,
        $MAIN_AUDIO_ENGLISHNAME text not null,
        $MAIN_AUDIO_HINDINAME text
       )''');

    await db.execute('''
      create table if not exists $table_SUB_AUDIO (
        $SUB__AUDIOID integer primary key ,
        $SUB_MAIN_AUDIOID integer,
         $SUB_AUDIO_ENGLISHNAME text not null,
        $SUB_AUDIO_HINDINAME text
       )''');

    await db.execute('''
      create table if not exists $table_MAIN_Book (
        $MAIN_BOOK_ID integer primary key ,
         $MAIN_BOOK_ENGLISHNAME text not null,
        $MAIN_BOOK_HINDINAME text
       )''');

    await db.execute('''
      create table if not exists $table_CHAPTER_LIST (
        $CHAPTER_LIST_ID integer primary key ,
         $CHAPTER_BOOK_ID integer,
        $CHAPTER_LIST_ChapterNameEnglish text,
        $CHAPTER_LIST_ChapterNameHindi text,
        $CHAPTER_LIST_ChapterTextEnglish text,
         $CHAPTER_LIST_ChapterTextHindi text,
        $CHAPTER_LIST_ChapterMeaningTextEnglish text,
        $CHAPTER_LIST_ChapterMeaningTextHindi text,
        $CHAPTER_LIST_ChapterAudioPathEnglish text,
        $CHAPTER_LIST_ChapterChapterAudioPathHindi text,
        $CHAPTER_LIST_ChapterIsSubChapter text
       )''');

    await db.execute('''
      create table if not exists $table_SUB_CHAPTER_LIST (
        $SUB_CHAPTER_LIST_ID integer primary key ,
         $SUB_CHAPTER_BOOK_ID integer,
        $SUB_CHAPTER_LIST_ChapterNameEnglish text,
        $SUB_CHAPTER_LIST_ChapterNameHindi text,
        $SUB_CHAPTER_LIST_ChapterTextEnglish text,
         $SUB_CHAPTER_LIST_ChapterTextHindi text,
        $SUB_CHAPTER_LIST_ChapterMeaningTextEnglish text,
        $SUB_CHAPTER_LIST_ChapterMeaningTextHindi text,
        $SUB_CHAPTER_LIST_ChapterAudioPathEnglish text,
        $SUB_CHAPTER_LIST_ChapterChapterAudioPathHindi text
       )''');
  }

  /*todo----get book chapter with main book id*/
  Future<List<SubChapterList>> getBookSubChapter(int bookId) async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query(
        table_SUB_CHAPTER_LIST,
        where: "sub_chapter_bookId =?",
        whereArgs: [bookId]);

    return List.generate(maps.length, (i) {
      return SubChapterList(
          id: maps[i][SUB_CHAPTER_LIST_ID],
          chapterId: maps[i][SUB_CHAPTER_BOOK_ID],
          chapterNameEnglish: maps[i][SUB_CHAPTER_LIST_ChapterNameEnglish],
          chapterNameHindi: maps[i][SUB_CHAPTER_LIST_ChapterNameHindi],
          chapterTextHindi: maps[i][SUB_CHAPTER_LIST_ChapterTextHindi],
          chapterTextEnglish: maps[i][SUB_CHAPTER_LIST_ChapterTextEnglish],
          chapterMeaningTextEnglish: maps[i]
              [SUB_CHAPTER_LIST_ChapterMeaningTextEnglish],
          chapterMeaningTextHindi: maps[i]
              [SUB_CHAPTER_LIST_ChapterMeaningTextHindi],
          chapterAudioPathEnglish: maps[i]
              [SUB_CHAPTER_LIST_ChapterAudioPathEnglish],
          chapterAudioPathHindi: maps[i]
              [SUB_CHAPTER_LIST_ChapterChapterAudioPathHindi]);
    });
  }

// /*todo------insert  method*/
  Future<int> insert(String tablename, Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(tablename, row,
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  /*todo----for get all data*/
  Future<List<Map<String, dynamic>>> queryall() async {
    Database db = await instance.database;
    return await db.query(table_SUB_AUDIO);
  }

  Future<List<Map<String, dynamic>>> querySpecific(int ID_SUBAUDIO) async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query(table_SUB_AUDIO,
        where: "ID_SUBAUDIO =?", whereArgs: [ID_SUBAUDIO]);

    return maps;
  }

  /*todo---get main audio list*/
  Future<List<MainAudioList>> getMaiAudioList() async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query(tableMAINAUDIO);

    return List.generate(maps.length, (i) {
      return MainAudioList(
        id: maps[i][MAIN_AUDIOID],
        audioNameEnglish: maps[i][MAIN_AUDIO_ENGLISHNAME],
        audioNameHindi: maps[i][MAIN_AUDIO_HINDINAME],
      );
    });
  }

  //SubAudioTitle

  /*todo--get main book list*/

  Future<List<MainBookList>> getMainBook() async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query(table_MAIN_Book);

    return List.generate(maps.length, (i) {
      return MainBookList(
        id: maps[i][MAIN_BOOK_ID],
        bookNameEnglish: maps[i][MAIN_BOOK_ENGLISHNAME],
        bookNameHindi: maps[i][MAIN_BOOK_HINDINAME],
      );
    });
  }

  /*todo----get all sub Audio file*/

  Future<List<SubAudioTitle>> getSubAudioFile(int mainAudiId) async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query(table_SUB_AUDIO,
        where: "sub_mainaudio_id =?", whereArgs: [mainAudiId]);

    return List.generate(maps.length, (i) {
      return SubAudioTitle(
          id: maps[i][SUB__AUDIOID],
          mainAudioId: maps[i][SUB_MAIN_AUDIOID],
          audioNameEnglish: maps[i][SUB_AUDIO_ENGLISHNAME],
          audioNameHindi: maps[i][SUB_AUDIO_HINDINAME]);
    });
  }

  /*todo----get book chapter with main book id*/
  Future<List<ChapterOfBook>> getBookChapter(int bookId) async {
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query(table_CHAPTER_LIST,
        where: "chapter_bookId =?", whereArgs: [bookId]);

    return List.generate(maps.length, (i) {
      return ChapterOfBook(
        id: maps[i][CHAPTER_LIST_ID],
        bookId: maps[i][CHAPTER_BOOK_ID],
        chapterNameEnglish: maps[i][CHAPTER_LIST_ChapterNameEnglish],
        chapterNameHindi: maps[i][CHAPTER_LIST_ChapterNameHindi],
        chapterTextHindi: maps[i][CHAPTER_LIST_ChapterTextHindi],
        chapterTextEnglish: maps[i][CHAPTER_LIST_ChapterTextEnglish],
        chapterMeaningTextEnglish: maps[i]
            [CHAPTER_LIST_ChapterMeaningTextEnglish],
        chapterMeaningTextHindi: maps[i][CHAPTER_LIST_ChapterMeaningTextHindi],
        chapterAudioPathEnglish: maps[i][CHAPTER_LIST_ChapterAudioPathEnglish],
        chapterAudioPathHindi: maps[i]
            [CHAPTER_LIST_ChapterChapterAudioPathHindi],
        isSubChapter:
            maps[i][CHAPTER_LIST_ChapterIsSubChapter] == null ? false : true,
      );
    });
  }
}
