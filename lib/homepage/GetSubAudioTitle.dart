// To parse this JSON data, do
//
//     final getSubAudioTitle = getSubAudioTitleFromJson(jsonString);

import 'dart:convert';

GetSubAudioTitle getSubAudioTitleFromJson(String str) =>
    GetSubAudioTitle.fromJson(json.decode(str));

String getSubAudioTitleToJson(GetSubAudioTitle data) =>
    json.encode(data.toJson());

class GetSubAudioTitle {
  GetSubAudioTitle({
    this.message,
    this.data,
    this.userId,
  });

  String message;
  List<SubAudioTitle> data;
  int userId;

  factory GetSubAudioTitle.fromJson(Map<String, dynamic> json) =>
      GetSubAudioTitle(
        message: json["Message"],
        data: List<SubAudioTitle>.from(
            json["Data"].map((x) => SubAudioTitle.fromJson(x))),
        userId: json["userId"],
      );

  Map<String, dynamic> toJson() => {
        "Message": message,
        "Data": List<dynamic>.from(data.map((x) => x.toJson())),
        "userId": userId,
      };
}

class SubAudioTitle {
  SubAudioTitle({
    this.id,
    this.mainAudioId,
    this.audioNameEnglish,
    this.audioNameHindi,
  });

  int id;
  int mainAudioId;
  String audioNameEnglish;
  dynamic audioNameHindi;

  factory SubAudioTitle.fromJson(Map<String, dynamic> json) => SubAudioTitle(
        id: json["ID"],
        mainAudioId: json["MainAudioID"],
        audioNameEnglish: json["AudioNameEnglish"],
        audioNameHindi: json["AudioNameHindi"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "MainAudioID": mainAudioId,
        "AudioNameEnglish": audioNameEnglish,
        "AudioNameHindi": audioNameHindi,
      };
}
