import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/BoxdecorationCommon.dart';
import 'package:flutter_app/audioplay/PlayAudioPage.dart';
import 'package:flutter_app/networkRequest/ApiUrl.dart';
import 'package:flutter_app/networkRequest/Webapi.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:flutter_app/sharedfile/databasehelper.dart';

import 'GetSubAudioTitle.dart';

class SubAudioListPage extends StatefulWidget {
  var audioId = "";

  SubAudioListPage(String audioId) {
    this.audioId = audioId;
  }

  @override
  _SubAudioListPageState createState() => _SubAudioListPageState(audioId);
}

class _SubAudioListPageState extends State<SubAudioListPage> {
  final dbhelper = DataBaseHelper.instance;
  Color color = Colors.grey;
  Color color1 = Colors.grey;
  Color textcolor1, textcolor2;
  GetSubAudioTitle _getSubAudioTitle;
  var TAG = "_SubAudioListPageState ";
  var audioId = "";
  var colorCode = "0";
  var hindiCode = "0";

  _SubAudioListPageState(String audioId) {
    this.audioId = audioId;
  }

  List<SubAudioTitle> mainaudiolist = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    PreferenceUtils.init();
    setState(() {
      hindiCode = PreferenceUtils.getString("hindicode").toString();
      colorCode = PreferenceUtils.getString("themeColor").toString();
    });

    color = Color(hexStringToHexInt("#2A406C"));
    textcolor1 = Colors.white;
    textcolor2 = Colors.black;
    Webapi().check().then((intenet) {
      if (intenet != null && intenet) {
        _getSubAudio(context);
      } else {
        debugPrint("No internet , fetch data from local");
        getSubAudiofromdb(audioId);
      }
    });

    debugPrint(
        "$TAG PreferenceUtils themeColor=${PreferenceUtils.getString("themeColor").toString()}");
    colorCode = PreferenceUtils.getString("themeColor").toString();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(hexStringToHexInt("#6C6C88")),
          centerTitle: true,
          title: Text(
            "Jinvani",
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.12,
                color: Colors.white,
                fontFamily: "Poorich"),
          ),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(Commonmethod.backgroundImage(colorCode)),
                fit: BoxFit.fill),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: <Widget>[
//                     Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Row(
//                         children: <Widget>[
//                           GestureDetector(
//                             onTap: () {
//                               setState(() {
//                                 Commonmethod.setData();
//                                 if (PreferenceUtils.getString("themeColor") == "0") {
//                                   PreferenceUtils.setString("themeColor", "1");
//                                   colorCode="1";
// //                                  debugPrint("$TAG Preference choosed color=$colorCode");
//                                 } else/* if (shareval == "1")*/ {
//                                   PreferenceUtils.setString("themeColor", "0");
//                                    colorCode="0";
//   //                                debugPrint("$TAG Preference choosed color=$colorCode");
//                                 }
//
//                                 print("$TAG PreferenceUtils="+PreferenceUtils.getString("themeColor"));
//                               });
//                             },
//                             child: Container(
//                               child: Material(
//                                 elevation: 10,
//                                 color: Color(hexStringToHexInt("#9F9CB7")),
//                                 shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.circular(5.0)),
//                                 child: Padding(
//                                   padding: const EdgeInsets.all(8.0),
//                                   child: Row(
//                                     children: <Widget>[
//                                       Icon(
//                                         Icons.menu,
//                                         color: Colors.black,
//                                       ),
//                                       Text(
//                                         "Menu",
//                                       )
//                                     ],
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           )
//                         ],
//                       ),
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Row(
//                         children: <Widget>[
//                           Material(
//                             color: Colors.grey,
//                             borderRadius:
//                                 BorderRadius.all(Radius.circular(5.0)),
//                             child: Padding(
//                               padding: const EdgeInsets.all(2.0),
//                               child: Container(
//                                   decoration: BoxDecoration(
//                                     color: Colors.transparent,
//                                   ),
//                                   child: Row(
//                                     children: <Widget>[
//                                       GestureDetector(
//                                         onTap: () {
//                                           setState(() {
//                                             color1 = Colors.grey;
//                                             color = Color(
//                                                 hexStringToHexInt("#2A406C"));
//                                             textcolor1 = Colors.white;
//                                             textcolor2 = Colors.black;
//                                           });
//                                         },
//                                         child: Container(
//                                           decoration: BoxDecoration(
//                                             color: color,
//                                           ),
//                                           child: Center(
//                                             child: Padding(
//                                               padding:
//                                                   const EdgeInsets.all(8.0),
//                                               child: Text(
//                                                 "English",
//                                                 style: TextStyle(
//                                                     color: textcolor2),
//                                               ),
//                                             ),
//                                           ),
//                                         ),
//                                       ),
//                                       GestureDetector(
//                                         onTap: () {
//                                           setState(() {
//                                             color1 = Color(
//                                                 hexStringToHexInt("#2A406C"));
//                                             color = Colors.grey;
//                                             textcolor1 = Colors.black;
//                                             textcolor2 = Colors.white;
//                                           });
//                                         },
//                                         child: Container(
//                                             decoration: BoxDecoration(
//                                               color: color1,
//                                             ),
//                                             child: Center(
//                                               child: Padding(
//                                                 padding:
//                                                     const EdgeInsets.all(8.0),
//                                                 child: Text(
//                                                   "Hindi",
//                                                   style: TextStyle(
//                                                       color: textcolor1),
//                                                 ),
//                                               ),
//                                             )),
//                                       )
//                                     ],
//                                   )),
//                             ),
//                           )
//                         ],
//                       ),
//                     )
//                   ],
//                 ),
                SizedBox(
                  height: MediaQuery.of(context).size.width * 0.2,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: _mainAudioListview(context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }

  void _getSubAudio(BuildContext context) async {
    showLoaderDialog(context);
    Map map = {"MainAudioID": audioId};
    String response =
        await Webapi().postWithbody(ApiUrl.Get_Sub_Audio_Titles, map);

    setState(() {
      Navigator.of(context).pop();
      var jsondata = json.decode(response);
      //    PreferenceUtils.setUserPojo("_getSubAudio", response);
      var message = jsondata['Message'];
      if (message.toString() == "Success") {
        debugPrint("AP DATA $response");
        _getSubAudioTitle = getSubAudioTitleFromJson(response);
        mainaudiolist = _getSubAudioTitle.data;

        for (int i = 0; i < mainaudiolist.length; i++) {
          saveSubAudio(
              mainaudiolist[i].id,
              mainaudiolist[i].mainAudioId,
              mainaudiolist[i].audioNameEnglish == null
                  ? ""
                  : mainaudiolist[i].audioNameEnglish,
              mainaudiolist[i].audioNameHindi == null
                  ? ""
                  : mainaudiolist[i].audioNameHindi);
        }
        SubAudioTitle title = SubAudioTitle(
            id: 2, mainAudioId: 3, audioNameEnglish: "", audioNameHindi: "");
      } else {
        debugPrint("ERROR ERROR $message");
      }
    });
  }

  Widget _mainAudioListview(context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          itemCount: mainaudiolist.length,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemBuilder: (context, position) {
            return Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.09,
                  right: MediaQuery.of(context).size.width * 0.09,
                  bottom: MediaQuery.of(context).size.width * 0.02),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PlayAudioPage(
                            mainaudiolist[position].id.toString())),
                  );
                },
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.1 - 22,
                  child: Text(
                    hindiCode == "0"
                        ? mainaudiolist[position].audioNameEnglish + ""
                        : mainaudiolist[position].audioNameHindi != null
                            ? mainaudiolist[position].audioNameHindi
                            : "N/A",
                    style: TextStyle(
                        fontFamily: "Roboto Regular",
                        fontSize: MediaQuery.of(context).size.width * 0.05,
                        color: Colors.white),
                  ),
                  decoration: Commonmethod.decoration(),
                ),
              ),
            );
          }),
    );
  }

  void saveSubAudio(int audioid, int subMainaudioid, String audioEnglishName,
      String audioHindiName) async {
    Map<String, dynamic> row = {
      DataBaseHelper.SUB__AUDIOID: audioid,
      DataBaseHelper.SUB_MAIN_AUDIOID: subMainaudioid,
      DataBaseHelper.SUB_AUDIO_ENGLISHNAME: audioEnglishName,
      DataBaseHelper.SUB_AUDIO_HINDINAME: audioHindiName
    };
    final id = await dbhelper.insert(DataBaseHelper.table_SUB_AUDIO, row);
    debugPrint("SAVE SUB AUDIO $id");
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void getSubAudiofromdb(audioId) async {
    debugPrint("AUDIO ID " + audioId);
    var res = await dbhelper.getSubAudioFile(int.parse(audioId));
    print(res);
    setState(() {
      mainaudiolist = res;
    });
  }
}
