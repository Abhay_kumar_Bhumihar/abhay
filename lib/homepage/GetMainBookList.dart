// To parse this JSON data, do
//
//     final getMainBookList = getMainBookListFromJson(jsonString);

import 'dart:convert';

GetMainBookList getMainBookListFromJson(String str) =>
    GetMainBookList.fromJson(json.decode(str));

String getMainBookListToJson(GetMainBookList data) =>
    json.encode(data.toJson());

class GetMainBookList {
  GetMainBookList({
    this.message,
    this.data,
    this.userId,
  });

  String message;
  List<MainBookList> data;
  int userId;

  factory GetMainBookList.fromJson(Map<String, dynamic> json) =>
      GetMainBookList(
        message: json["Message"],
        data: List<MainBookList>.from(
            json["Data"].map((x) => MainBookList.fromJson(x))),
        userId: json["userId"],
      );

  Map<String, dynamic> toJson() => {
        "Message": message,
        "Data": List<dynamic>.from(data.map((x) => x.toJson())),
        "userId": userId,
      };
}

class MainBookList {
  MainBookList({
    this.id,
    this.bookNameEnglish,
    this.bookNameHindi,
  });

  int id;
  String bookNameEnglish;
  String bookNameHindi;

  factory MainBookList.fromJson(Map<String, dynamic> json) => MainBookList(
        id: json["ID"],
        bookNameEnglish: json["BookNameEnglish"],
        bookNameHindi: json["BookNameHindi"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "BookNameEnglish": bookNameEnglish,
        "BookNameHindi": bookNameHindi,
      };
}
