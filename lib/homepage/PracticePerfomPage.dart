import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/BoxdecorationCommon.dart';
import 'package:flutter_app/chapter/ChapterWithOutMeaning.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PracticePerform extends StatefulWidget {
  @override
  _PracticePerformState createState() => _PracticePerformState();
}

class _PracticePerformState extends State<PracticePerform> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: Color(hexStringToHexInt("#6C6C88")),
        centerTitle: true,
        title: Text(
          "Jinvani",
          style: TextStyle(
              fontSize: MediaQuery.of(context).size.width * 0.12,
              color: Colors.white,
              fontFamily: "Poorich"),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(Commonmethod.backgroundImage("0")),
              fit: BoxFit.fill),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Commonmethod.logowithText(Colors.black, context),
              SizedBox(
                height: MediaQuery.of(context).size.width * 0.1,
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.09,
                    right: MediaQuery.of(context).size.width * 0.09,
                    bottom: MediaQuery.of(context).size.width * 0.02),
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.1 - 22,
                  child: Text(
                    "To Practice",
                    style: TextStyle(
                        fontSize: MediaQuery.of(context).size.width * 0.05,
                        color: Colors.white,
                        fontFamily: "Roboto Regular"),
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      color: Color(hexStringToHexInt("#6C6C86")),
                      border: Border.all(
                          color: Color(hexStringToHexInt("#5C6D8F")))),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.09,
                    right: MediaQuery.of(context).size.width * 0.09,
                    bottom: MediaQuery.of(context).size.width * 0.02),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ChapterWithOutMeaning()),
                    );
                  },
                  child: Container(
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.1 - 22,
                    child: Text(
                      "To Perform",
                      style: TextStyle(
                          fontSize: MediaQuery.of(context).size.width * 0.05,
                          color: Colors.white,
                          fontFamily: "Roboto Regular"),
                    ),
                    decoration: Commonmethod.decoration(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  getTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String stringValue = prefs.getString('theme');
    return stringValue;
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }

  String getBackgroundAssetName() {
    if (getTheme() != null) {
      if (getTheme() == "0") return "assets/blue_background.png";
      if (getTheme() == "1") return "assets/white_background.png";
    }
    return 'assets/blue_background.png';
  }

  BoxDecoration decoration() {
    return BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: Color(hexStringToHexInt("#6C6C86")),
        border: Border.all(color: Color(hexStringToHexInt("#5C6D8F"))));
  }
}
