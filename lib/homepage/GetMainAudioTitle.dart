// To parse this JSON data, do
//
//     final getMainAudioTitle = getMainAudioTitleFromJson(jsonString);

import 'dart:convert';

GetMainAudioTitle getMainAudioTitleFromJson(String str) =>
    GetMainAudioTitle.fromJson(json.decode(str));

String getMainAudioTitleToJson(GetMainAudioTitle data) =>
    json.encode(data.toJson());

class GetMainAudioTitle {
  GetMainAudioTitle({
    this.message,
    this.data,
    this.userId,
  });

  String message;
  List<MainAudioList> data;
  int userId;

  factory GetMainAudioTitle.fromJson(Map<String, dynamic> json) =>
      GetMainAudioTitle(
        message: json["Message"],
        data: List<MainAudioList>.from(
            json["Data"].map((x) => MainAudioList.fromJson(x))),
        userId: json["userId"],
      );

  Map<String, dynamic> toJson() => {
        "Message": message,
        "Data": List<dynamic>.from(data.map((x) => x.toJson())),
        "userId": userId,
      };
}

class MainAudioList {
  MainAudioList({
    this.id,
    this.audioNameEnglish,
    this.audioNameHindi,
  });

  int id;
  String audioNameEnglish;
  dynamic audioNameHindi;

  factory MainAudioList.fromJson(Map<String, dynamic> json) => MainAudioList(
        id: json["ID"],
        audioNameEnglish: json["AudioNameEnglish"],
        audioNameHindi: json["AudioNameHindi"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "AudioNameEnglish": audioNameEnglish,
        "AudioNameHindi": audioNameHindi,
      };
}
