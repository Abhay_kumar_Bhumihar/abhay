import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/BoxdecorationCommon.dart';
import 'package:flutter_app/ChapterListFile/BookChapter.dart';
import 'package:flutter_app/homepage/SubAudioListPage.dart';
import 'package:flutter_app/networkRequest/ApiUrl.dart';
import 'package:flutter_app/networkRequest/Webapi.dart';
import 'package:flutter_app/sharedfile/PreferenceUtils.dart';
import 'package:flutter_app/sharedfile/databasehelper.dart';

import '../MalaPage.dart';
import 'GetMainAudioTitle.dart';
import 'GetMainBookList.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final dbhelper = DataBaseHelper.instance;
  var TAG = "_HomePageState ";
  var colorCode = "0";
  var hindiCode = "0";

  Color color = Colors.grey;
  Color color1 = Colors.grey;
  Color textcolor1, textcolor2;
  GetMainAudioTitle _getMainAudioTitle;
  GetMainBookList _getMainBookList;
  List<MainBookList> _mainBooklist = [];

  List<MainAudioList> mainaudiolist = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    PreferenceUtils.init();
    debugPrint("$TAG shared pref themeColor=" +
        PreferenceUtils.getString("themeColor").toString());
    setState(() {
      hindiCode = PreferenceUtils.getString("hindicode").toString();
      colorCode = PreferenceUtils.getString("themeColor").toString();
    });

    color = Color(hexStringToHexInt("#2A406C"));
    textcolor1 = Colors.white;
    textcolor2 = Colors.black;
    Webapi().check().then((intenet) {
      if (intenet != null && intenet) {
        getMainAudioList(context);
      } else {
        debugPrint("No internet , fetch data from local");
        getAllMainAudiofromdb();
        getMainBookfromdb();
      }
    });

    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarColor: Colors.white,
    // ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(hexStringToHexInt("#6C6C88")),
          centerTitle: true,
          title: Text(
            "Jinvani",
            style: TextStyle(
                fontSize: MediaQuery.of(context).size.width * 0.12,
                color: Colors.white,
                fontFamily: "Poorich"),
          ),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MalaPge(),
                    ));
              },
              iconSize: 34.0,
              icon: Icon(Icons.adjust_outlined),
              color: Colors.cyan,
            ),
          ],
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(Commonmethod.backgroundImage(colorCode)),
                fit: BoxFit.fill),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                //  Commonmethod.setData();
                                var shareval =
                                PreferenceUtils.getString("themeColor")
                                    .toString();
                                print("$TAG  themeColor $shareval");

                                if (shareval == "0") {
                                  PreferenceUtils.setString("themeColor", "1");
                                  colorCode = "1";
                                  debugPrint("111111111111");
                                } else
                                  /* if (shareval == "1")*/ {
                                  PreferenceUtils.setString("themeColor", "0");
                                  debugPrint("OOOOOOOOOO");
                                  colorCode = "0";
                                }
                                print("sfsdfsdf  " +
                                    PreferenceUtils.getString("themeColor")
                                        .toString());
                              });
                            },
                            child: Container(
                                height: 44,
                                width: 44,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage('assets/contrast.png'),
                                      fit: BoxFit.cover),
                                )),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          Material(
                            color: Colors.grey,
                            borderRadius:
                            BorderRadius.all(Radius.circular(5.0)),
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            PreferenceUtils.setString(
                                                "hindicode", "0");
                                            hindiCode = "0";
                                            color1 = Colors.grey;
                                            color = Color(
                                                hexStringToHexInt("#2A406C"));
                                            textcolor1 = Colors.white;
                                            textcolor2 = Colors.black;
                                          });
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: color,
                                          ),
                                          child: Center(
                                            child: Padding(
                                              padding:
                                              const EdgeInsets.all(8.0),
                                              child: Text(
                                                "English",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            PreferenceUtils.setString(
                                                "hindicode", "1");
                                            hindiCode = "1";
                                            color1 = Color(
                                                hexStringToHexInt("#2A406C"));
                                            color = Colors.grey;
                                            textcolor1 = Colors.black;
                                            textcolor2 = Colors.white;
                                          });
                                        },
                                        child: Container(
                                            decoration: BoxDecoration(
                                              color: color1,
                                            ),
                                            child: Center(
                                              child: Padding(
                                                padding:
                                                const EdgeInsets.all(8.0),
                                                child: Text(
                                                  "Hindi",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                              ),
                                            )),
                                      )
                                    ],
                                  )),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.width * 0.2,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: _mainAudioListview(context),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: _mainBookListView(context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  hexStringToHexInt(String hex) {
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    return val;
  }

  void getMainAudioList(BuildContext context) async {
    showLoaderDialog(context);
    String response =
    await Webapi().postWithoutbody(ApiUrl.Get_Main_Audios_Title);

    setState(() {
      Navigator.of(context).pop();
      var jsondata = json.decode(response);
      var message = jsondata['Message'];
      if (message.toString() == "Success") {
        debugPrint("AP DATA $response");

        /*todo----save data in local db*/
        //  PreferenceUtils.setUserPojo("_getMainAudioTitle", response);
        _getMainAudioTitle = getMainAudioTitleFromJson(response);
        mainaudiolist = _getMainAudioTitle.data;
        for (int i = 0; i < mainaudiolist.length; i++) {
          saveMainAudiList(
              mainaudiolist[i].id,
              mainaudiolist[i].audioNameEnglish == null
                  ? ""
                  : mainaudiolist[i].audioNameEnglish,
              mainaudiolist[i].audioNameHindi == null
                  ? ""
                  : mainaudiolist[i].audioNameHindi);
        }
      } else {
        debugPrint("ERROR ERROR $message");
      }

      getMainBookList();
    });
  }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void getMainBookList() async {
    String response = await Webapi().postWithoutbody(ApiUrl.Get_Main_Book_List);
    setState(() {
      var jsondata = json.decode(response);
      var message = jsondata['Message'];
      if (message.toString() == "Success") {
        /*todo----save data in local db*/

        // PreferenceUtils.setUserPojo("getMainBookList", response);
        debugPrint("AP DATA $response");
        _getMainBookList = getMainBookListFromJson(response);
        _mainBooklist = _getMainBookList.data;

        for (int i = 0; i < _mainBooklist.length; i++) {
          saveMainBookList(
              _mainBooklist[i].id,
              _mainBooklist[i].bookNameEnglish == null
                  ? ""
                  : _mainBooklist[i].bookNameEnglish,
              _mainBooklist[i].bookNameHindi == null
                  ? ""
                  : _mainBooklist[i].bookNameHindi);
        }
      } else {
        debugPrint("ERROR ERROR $message");
      }
    });
  }

  Widget _mainAudioListview(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          itemCount: mainaudiolist.length,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemBuilder: (context, position) {
            return Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.09,
                  right: MediaQuery.of(context).size.width * 0.09,
                  bottom: MediaQuery.of(context).size.width * 0.02),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SubAudioListPage(
                            mainaudiolist[position].id.toString())),
                  );
                },
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.1 - 22,
                  child: Text(
                    hindiCode == "0"
                        ? mainaudiolist[position].audioNameEnglish + ""
                        : mainaudiolist[position].audioNameHindi != null
                        ? mainaudiolist[position].audioNameHindi
                        : "N/A",
                    style: TextStyle(
                        fontFamily: "Roboto Regular",
                        fontSize: MediaQuery.of(context).size.width * 0.05,
                        color: Colors.white),
                  ),
                  decoration: Commonmethod.decoration(),
                ),
              ),
            );
          }),
    );
  }

  Widget _mainBookListView(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          itemCount: _mainBooklist.length,
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemBuilder: (context, position) {
            return Padding(
              padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.09,
                  right: MediaQuery.of(context).size.width * 0.09,
                  bottom: MediaQuery.of(context).size.width * 0.02),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            BookChapter(_mainBooklist[position].id.toString())),
                  );
                },
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.1 - 22,
                  /*
                   _mainBooklist[position].bookNameEnglish + ""
                  ,*/
                  child: Text(
                    hindiCode == "0"
                        ? _mainBooklist[position].bookNameEnglish + ""
                        : _mainBooklist[position].bookNameHindi != null
                        ? _mainBooklist[position].bookNameHindi
                        : "N/A",
                    style: TextStyle(
                        fontFamily: "Roboto Regular",
                        fontSize: MediaQuery.of(context).size.width * 0.05,
                        color: Colors.white),
                  ),
                  decoration: Commonmethod.decoration(),
                ),
              ),
            );
          }),
    );
  }

  void saveMainAudiList(
      int audioid, String audioEnglishName, String audioHindiName) async {
    Map<String, dynamic> row = {
      DataBaseHelper.MAIN_AUDIOID: audioid,
      DataBaseHelper.MAIN_AUDIO_ENGLISHNAME: audioEnglishName,
      DataBaseHelper.MAIN_AUDIO_HINDINAME: audioHindiName
    };
    final id = await dbhelper.insert(DataBaseHelper.tableMAINAUDIO, row);
    debugPrint("SAVE MAIN AUDIO $id");
  }

  void saveMainBookList(
      int bookid, String bookenglishname, String bookhindiname) async {
    Map<String, dynamic> row = {
      DataBaseHelper.MAIN_BOOK_ID: bookid,
      DataBaseHelper.MAIN_BOOK_ENGLISHNAME: bookenglishname,
      DataBaseHelper.MAIN_BOOK_HINDINAME: bookhindiname
    };
    var id = await dbhelper.insert(DataBaseHelper.table_MAIN_Book, row);
    debugPrint("BOOK SAVE $id");
  }

  void getAllMainAudiofromdb() async {
    var res = await dbhelper.getMaiAudioList();
    setState(() {
      mainaudiolist = res;
      debugPrint("sdkfsdkfkldfklsdjfklsdjf  $res");
      for (int i = 0; i < res.length; i++) {
        debugPrint(res[i].id.toString() +
            " " +
            res[i].audioNameEnglish +
            " " +
            res[i].audioNameHindi +
            "");
      }
    });
  }

  void getMainBookfromdb() async {
    var res = await dbhelper.getMainBook();
    setState(() {
      _mainBooklist = res;
    });
  }
}
