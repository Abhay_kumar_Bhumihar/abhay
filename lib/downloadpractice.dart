import 'dart:io';
import 'dart:async';
import 'package:audioplayer/audioplayer.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;

import 'package:path_provider/path_provider.dart';

// void main(){
//   runApp(new MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: new FileDownloadView()
//
//   ));
// }

class FileDownloadView extends StatefulWidget {
  @override
  _FileDownloadViewState createState() => _FileDownloadViewState();
}

enum PlayerState { stopped, playing, paused }

class _FileDownloadViewState extends State<FileDownloadView> {
  String _filePath = "";
  AudioPlayer audioPlayer;
  PlayerState playerState = PlayerState.stopped;
  Duration duration;
  Duration position;

  get isPlaying => playerState == PlayerState.playing;

  get isPaused => playerState == PlayerState.paused;

  get durationText =>
      duration != null ? duration.toString().split('.').first : '';

  get positionText =>
      position != null ? position.toString().split('.').first : '';

  bool isMuted = false;

  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;

  Future<String> get _localDevicePath async {
    final _devicePath = await getExternalStorageDirectory();
    return _devicePath.path;
  }

  @override
  void initState() {
    super.initState();
    initAudioPlayer();
  }

  void initAudioPlayer() {
    audioPlayer = AudioPlayer();
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => position = p));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        setState(() => duration = audioPlayer.duration);
      } else if (s == AudioPlayerState.STOPPED) {
        //onComplete();
        setState(() {
          position = duration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = PlayerState.stopped;
        duration = Duration(seconds: 0);
        position = Duration(seconds: 0);
      });
    });
  }

  Future<File> _localFile({String path, String type}) async {
    String _path = await _localDevicePath;

  //  var _newPath = await Directory("/storage/emulated/0/$path").create();
    var _newPath = await Directory("/storage/emulated/0/Android/data/$path").create();
    localFilePath = "${_newPath.path}/pdf.$type";
    return File("${_newPath.path}/pdf.$type");
  }///storage/emulated/0/Android/data/abhayfilepdf.mp3

  Future _playLocal() async {
    await audioPlayer.play(localFilePath, isLocal: true);
    setState(() => playerState = PlayerState.playing);
  }

  // void initPlayer() {
  //   advancedPlayer = new AudioPlayer();
  //   audioCache = new AudioCache(fixedPlayer: advancedPlayer);
  //
  //   advancedPlayer.durationHandler = (d) => setState(() {
  //         _duration = d;
  //       });
  //
  //   advancedPlayer.positionHandler = (p) => setState(() {
  //         _position = p;
  //       });
  // }

  String localFilePath;

  //
  // Widget _tab(List<Widget> children) {
  //   return Center(
  //     child: Container(
  //       padding: EdgeInsets.all(16.0),
  //       child: Column(
  //         children: children
  //             .map((w) => Container(child: w, padding: EdgeInsets.all(6.0)))
  //             .toList(),
  //       ),
  //     ),
  //   );
  // }
  //
  // Widget _btn(String txt, VoidCallback onPressed) {
  //   return ButtonTheme(
  //       minWidth: 48.0,
  //       child: RaisedButton(child: Text(txt), onPressed: onPressed));
  // }
  //
  // Widget slider() {
  //   return Slider(
  //       value: _position.inSeconds.toDouble(),
  //       min: 0.0,
  //       max: _duration.inSeconds.toDouble(),
  //       onChanged: (double value) {
  //         setState(() {
  //           seekToSecond(value.toInt());
  //           value = value;
  //         });
  //       });
  // }
  //
  // Widget localAsset() {
  //   return _tab([
  //     Text('Play Local Asset \'audio.mp3\':'),
  //     _btn('Play', () => audioCache.play('audio.mp3')),
  //     _btn('Pause', () => advancedPlayer.pause()),
  //     _btn('Stop', () => advancedPlayer.stop()),
  //     slider()
  //   ]);
  // }
  //
  // void seekToSecond(int second) {
  //   Duration newDuration = Duration(seconds: second);
  //
  //   advancedPlayer.seek(newDuration);
  // }

  // Future _downloadSamplePDF() async {
  //   final _response =
  //   await http.get("https://firebasestorage.googleapis.com/v0/b/text-recognition-28371.appspot.com/o/Direct%20Recruitment%202020.pdf?alt=media&token=114498c1-0e84-48e0-bda0-15d4b688f745");
  //   if (_response.statusCode == 200) {
  //     final _file = await _localFile(path: "MyApp", type: "pdf");
  //     final _saveFile = await _file.writeAsBytes(_response.bodyBytes);
  //
  //     Logger().i("File write complete. File Path ${_saveFile.path}");
  //     setState(() {
  //       _filePath = _saveFile.path;
  //     });
  //   } else {
  //     Logger().e(_response.statusCode);
  //   }
  // }

  Future _downloadSampleVideo() async {
    Logger().w("File write complete. File Path");
    final _response = await http.get(Uri.parse(
        "https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba.mp3"));
    if (_response.statusCode == 200) {
      final _file = await _localFile(type: "mp3", path: "abhayfile");
      final _saveFile = await _file.writeAsBytes(_response.bodyBytes);
      Logger().w("File write complete. File Path ${_saveFile.path}");
      setState(() {
        _filePath = _saveFile.path;
      });
    } else {
      Logger().e(_response.statusCode);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.file_download),
              label: Text("Sample Pdf"),
              onPressed: () {
                //  _downloadSamplePDF();
              },
            ),
            FlatButton.icon(
              icon: Icon(Icons.file_download),
              label: Text("Sample Videos"),
              onPressed: () {
                _downloadSampleVideo();
              },
            ),
            Text(_filePath),
            FlatButton.icon(
              icon: Icon(Icons.shop_two),
              label: Text("Show"),
              onPressed: () async {
                final _openFile = await OpenFile.open(_filePath);
                Logger().i(_openFile);
              },
            ),
            FlatButton.icon(
              icon: Icon(Icons.shop_two),
              label: Text("PLAY"),
              onPressed: () async {
                _playLocal();
              },
            ),

            // DefaultTabController(
            //   length: 1,
            //   child: Scaffold(
            //     appBar: AppBar(
            //       bottom: TabBar(
            //         tabs: [
            //           Tab(text: 'Local Asset'),
            //         ],
            //       ),
            //       title: Text('audioplayers Example'),
            //     ),
            //     body: TabBarView(
            //       children: [localAsset()],
            //     ),
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
